using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;

namespace Core {
    public interface ICameraMgr {
        Vector3 CameraViewCenterPosition { get; }
        void Shake(int preset);
        void Shake(ShakePreset preset);

        void AddFollowTarget(Transform target, float targetInfluenceH = 1f, float targetInfluenceV = 1f, float duration = 0f,
            Vector2 targetOffset = default(Vector2));

        void RemoveFollowTarget(Transform target, float duration = 0f);
    }
}