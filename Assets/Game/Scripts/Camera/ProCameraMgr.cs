using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;

namespace Core
{
    public class ProCameraMgr : ICameraMgr, IModule
    {
        public int Priority => 100;
        private ProCamera2D m_Camera;
        private ProCamera2DShake m_CameraShake;
        private Transform m_CamTransform;

        public Vector3 CameraViewCenterPosition => m_CamTransform.position;

        public void OnInit()
        {
            var proCamera = Object.FindObjectOfType<ProCamera2D>();
            if (proCamera == null) return;

            m_Camera = proCamera;
            m_CamTransform = m_Camera.transform;
            m_CameraShake = m_Camera.GetComponent<ProCamera2DShake>();
        }
        public void OnClose()
        {

        }
        public void OnUpdate()
        {

        }

        public void Shake(int preset)
        {
            if (m_CameraShake == null) return;
            m_CameraShake.Shake(preset);
        }

        public void Shake(ShakePreset preset)
        {
            if (m_CameraShake == null) return;
            m_CameraShake.Shake(preset);
        }

        public void AddFollowTarget(Transform target, float targetInfluenceH = 1, float targetInfluenceV = 1, float duration = 0,
            Vector2 targetOffset = default(Vector2))
        {
            if (m_Camera == null) return;
            m_Camera.AddCameraTarget(target, targetInfluenceH, targetInfluenceV, duration, targetOffset);
        }

        public void RemoveFollowTarget(Transform target, float duration = 0)
        {
            if (m_Camera == null) return;
            m_Camera.RemoveCameraTarget(target, duration);
        }
    }
}