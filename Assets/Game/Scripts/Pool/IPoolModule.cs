﻿using Lean.Pool;
using UnityEngine;

public interface IPoolModule : IModule
{
    public void Clear(GameObject clone);
    public void Despawn(Component clone, float delay = 0);
    public void Despawn(GameObject clone, float delay = 0);
    public void DespawnAll();
    public GameObject Spawn(GameObject prefab);
    public GameObject Spawn(GameObject prefab, Transform parent);
    public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion quaternion);
    public T Spawn<T>(T prefab) where T : Component;
    public T Spawn<T>(T prefab, Transform parent) where T : Component;
}