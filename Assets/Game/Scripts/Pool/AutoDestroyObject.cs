﻿using UnityEngine;
using UnityEngine.Events;

public class AutoDestroyObject : MonoBehaviour
{
    [SerializeField] private float timeDestroy = 3f;
    [SerializeField] private UnityEvent DestroyEvent;
    [SerializeField] private bool isAutoDestroy = false;

    public event System.Action onComplete;
    private bool isDestroying = false;
    private float time = 0;
    public void SetDuration(float duration)
    {
        onComplete = null;
        timeDestroy = duration;
        time = 0;
        isDestroying = true;
    }
    private void OnEnable()
    {
        time = 0;
    }
    private void OnDisable()
    {
        isDestroying = false;
    }
    private void Update()
    {
        if (!isAutoDestroy)
        {
            if (!isDestroying) return;
        }
        time += Time.deltaTime;

        if (time > timeDestroy)
        {
            onComplete?.Invoke();
            DestroyEvent?.Invoke();
            GameCore.Pool.Despawn(gameObject);
        }
    }
}