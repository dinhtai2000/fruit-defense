using System;
using Lean.Pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PoolManager : IPoolModule
{
    public int Priority => 1;

    public void OnInit()
    {
        SceneManager.activeSceneChanged += OnActiveSceneChanged;
    }

    public void OnClose()
    {
        SceneManager.activeSceneChanged -= OnActiveSceneChanged;
    }

    private void OnActiveSceneChanged(Scene curScene, Scene nextScene)
    {
        DespawnAll();
    }

    public T Spawn<T>(T prefab) where T : Component
    {
        return LeanPool.Spawn(prefab);
    } 
    

    public T Spawn<T>(T prefab, Transform parent) where T : Component
    {
        return LeanPool.Spawn(prefab, parent);
    }

    public GameObject Spawn(GameObject prefab)
    {
        return LeanPool.Spawn(prefab);
    }
    public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion quaternion)
    {
        var obj = LeanPool.Spawn(prefab);
        obj.transform.SetPositionAndRotation(position, quaternion);
        return obj;
    }

    public GameObject Spawn(GameObject prefab, Transform parent)
    {
        return LeanPool.Spawn(prefab, parent);
    }

    public void Despawn(Component clone, float delay = 0)
    {
        LeanPool.Despawn(clone, delay);
    }

    public void Despawn(GameObject clone, float delay = 0)
    {
        LeanPool.Despawn(clone, delay);
    }

    public void Clear(GameObject clone)
    {
        if (LeanPool.Links.ContainsKey(clone))
        {
            LeanPool.Links[clone].Clean();
        }
    }
    public void DespawnAll()
    {
        LeanPool.DespawnAll();
    }

    public void OnUpdate()
    {
    }
}
