﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static object _lock = new object();
    protected static T instance;
    protected static bool IsInitialized = false;
    public static T Instance
    {
        get
        {
            lock (_lock)
            {
                if (!instance) instance = FindObjectOfType<T>();
                if (!instance)
                {
                    var type = typeof(T);
                    instance = new GameObject(type.ToString()).AddComponent<T>();
                    (instance as MonoSingleton<T>).InitSingleton();
                }
            }
            return instance;
        }
    }

    protected virtual void InitSingleton()
    {
        IsInitialized = true;
    }
    protected virtual void Awake()
    {
        instance = GetComponent<T>();
        if (!IsInitialized)
        {
            InitSingleton();
        }
    }
    protected virtual void OnDestroy()
    {
        instance = null;
        IsInitialized = false;
    }
}