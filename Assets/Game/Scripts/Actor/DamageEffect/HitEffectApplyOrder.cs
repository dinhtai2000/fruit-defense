using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitEffectApplyOrder {
    BEFORE = 0,
    AFTER = 1
}