using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

public class StatusDamageEffect : DamageEffect {
    [SerializeField] private GameObject m_StatusPrefab;
    [SerializeField] private bool m_Forced;

    public override HitEffectApplyOrder ApplyOrder {
        get => HitEffectApplyOrder.BEFORE;
    }

    public override void Apply(Actor attacker, Actor defender) {
        if (attacker == null || defender == null) return;
        if (Utility.Random.RandomBoolean(Chance)) {
            defender.Status.AddStatus(attacker, m_StatusPrefab, m_Forced);
        }
    }
}