using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class DamageEffect : MonoBehaviour {
    [SerializeField, Range(0f, 1f)] private float m_Chance = 1f;
    [ShowInInspector] public abstract HitEffectApplyOrder ApplyOrder { get; }

    public float Chance {
        set => m_Chance = value;
        get => m_Chance;
    }

    public abstract void Apply(Actor attacker, Actor defender);
}