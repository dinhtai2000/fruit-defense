﻿public interface IInput
{
    void Init(Actor actor);
    void OnUpdate();

    void SetActive(bool active);
}