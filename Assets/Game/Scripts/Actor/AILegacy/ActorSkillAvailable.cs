﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorSkillAvailable", menuName = "Game/AI/ActorSkillAvailable")]
public class ActorSkillAvailable : Game.AI.BrainDecision
{
    public override bool Decide(Actor actor)
    {
        if (actor.IsDead) return false;
        return actor.Skill.IsSkillAvailable;
    }
}
