﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorHasTarget", menuName = "Game/AI/ActorHasTarget")]
public class ActorHasTarget : Game.AI.BrainDecision
{
    public override bool Decide(Actor actor)
    {
        return actor.TargetFinder.CurrentTarget != null;
    }
}
