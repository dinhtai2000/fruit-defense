using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorIsDead", menuName = "Game/AI/ActorIsDead")]
public class ActorIsDead : Game.AI.BrainDecision
{
    public override bool Decide(Actor actor)
    {
        if (actor.IsDead) return true;
        actor.IsDead = (actor.Health.CurrentHealth <= 0f);
        return actor.IsDead;
    }
}
