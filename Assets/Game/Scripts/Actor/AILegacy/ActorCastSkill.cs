﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorCastSkill", menuName = "Game/AI/ActorCastSkill")]
public class ActorCastSkill : Game.AI.BrainDecision
{
    public override bool Decide(Actor actor)
    {
        if (actor.IsDead) return false;
        return actor.Skill.CastSkillRandom();
    }
}
