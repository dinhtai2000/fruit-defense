﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorIsMoving", menuName = "Game/AI/ActorIsMoving")]
public class ActorIsMoving : Game.AI.BrainDecision
{
    public override bool Decide(Actor actor)
    {
        if (actor.IsDead) return false;
        return actor.Movement.IsMoving;
    }
}
