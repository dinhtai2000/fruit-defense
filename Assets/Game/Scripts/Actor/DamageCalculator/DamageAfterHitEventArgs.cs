using System.Collections;
using System.Collections.Generic;
using Core;
using Game.Core;
using UnityEngine;

public class DamageAfterHitEventArgs : BaseEventArgs<DamageAfterHitEventArgs> {
    public Actor Attacker;
    public Actor Defender;
    public DamageSource DamageSource;
    public HitResult HitResult;

    public DamageAfterHitEventArgs() {
        Attacker = null;
        Defender = null;
        DamageSource = null;
        HitResult = HitResult.FailedResult;
    }

    public static DamageAfterHitEventArgs Create(Actor _attacker, Actor _defender, DamageSource _source, HitResult _hitResult) {
        var eventArgs = new DamageAfterHitEventArgs();
        eventArgs.Attacker = _attacker;
        eventArgs.Defender = _defender;
        eventArgs.DamageSource = _source;
        eventArgs.HitResult = _hitResult;
        return eventArgs;
    }
}