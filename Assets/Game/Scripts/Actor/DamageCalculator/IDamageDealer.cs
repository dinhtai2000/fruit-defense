using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageDealer {
    void Init(IStatGroup stat);
    void Release(IStatGroup stat);
    HitResult DealDamage(Actor attacker, Actor defender);
}