using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable, HideLabel, FoldoutGroup("Damage Dealer")]
public class DamageDealer : IDamageDealer {
    [SerializeField] private DamageSource m_Source;

    [SerializeField] private bool m_ApplyEffect = true;

    [SerializeField] private List<DamageEffect> m_DamageEffects;

    public DamageSource DamageSource => m_Source;

    public Actor Owner { set; get; }

    public DamageDealer() {
        m_Source = new DamageSource();
    }

    public void Init(IStatGroup stat) {
        stat.AddListener(StatKey.Damage, OnUpdateBaseDamage);
    }

    public void Release(IStatGroup stat) {
        stat.RemoveListener(StatKey.Damage, OnUpdateBaseDamage);
    }

    private void OnUpdateBaseDamage(float value) {
        m_Source.Value = value;
    }

    public HitResult DealDamage(Actor attacker, Actor defender) {
        if (defender.IsDead) {
            return HitResult.FailedResult;
        }

        if (m_ApplyEffect && m_DamageEffects != null) {
            foreach (var effect in m_DamageEffects) {
                if (effect.ApplyOrder == HitEffectApplyOrder.BEFORE) effect.Apply(attacker, defender);
            }
        }

        HitResult hitResult = defender.DamageCalculator.CalculateDamage(defender, attacker, m_Source);

        if (m_ApplyEffect && hitResult.Success && m_DamageEffects != null) {
            foreach (var effect in m_DamageEffects) {
                if (effect.ApplyOrder == HitEffectApplyOrder.AFTER) effect.Apply(attacker, defender);
            }
        }

        return hitResult;
    }

    public void AddEffect(DamageEffect effect) {
        if (!m_DamageEffects.Contains(effect)) {
            m_DamageEffects.Add(effect);
        }
    }

    public void RemoveEffect(DamageEffect effect) {
        m_DamageEffects.Remove(effect);
    }

    public void CopyData(DamageDealer damageDealer) {
        m_Source.Value = damageDealer.DamageSource.Value;
        m_Source.Type = damageDealer.DamageSource.Type;
        m_Source.Method = damageDealer.DamageSource.Method;
        m_Source.DamageHealthPercentage = damageDealer.DamageSource.DamageHealthPercentage;
        m_Source.CannotEvade = damageDealer.DamageSource.CannotEvade;
        m_Source.CannotHurt = damageDealer.DamageSource.CannotHurt;
        Owner = damageDealer.Owner;

        if (damageDealer.m_DamageEffects == null) return;
        foreach (var effect in damageDealer.m_DamageEffects) {
            if (!m_DamageEffects.Contains(effect)) {
                m_DamageEffects.Add(effect);
            }
        }
    }
}