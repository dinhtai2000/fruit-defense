using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;

public class DamageCalculator : MonoBehaviour, IDamageCalculator {
    private readonly Dictionary<DamageType, bool> m_ImmuneDict = new Dictionary<DamageType, bool>();
    private readonly Dictionary<MethodType, bool> m_ImmuneMethodDict = new Dictionary<MethodType, bool>();
    private readonly DamageDealer m_ReflectDamageDealer = new DamageDealer();
    private const float MinDamage = 1f;
    private readonly TemporaryModifier m_AttackerTempMods = new TemporaryModifier();
    private readonly TemporaryModifier m_DefenderTempMods = new TemporaryModifier();

    public Actor Owner { get; private set; }

    public void Init(Actor actor) {
        Owner = actor;
        m_ReflectDamageDealer.DamageSource.Method = MethodType.REFLECT;
    }

    public void SetImmuneDamageType(DamageType type, bool immune) {
        if (!m_ImmuneDict.ContainsKey(type)) {
            m_ImmuneDict.Add(type, false);
        }

        m_ImmuneDict[type] = immune;
    }

    public void SetImmuneDamageMethod(MethodType method, bool immune) {
        if (!m_ImmuneMethodDict.ContainsKey(method)) {
            m_ImmuneMethodDict.Add(method, false);
        }

        m_ImmuneMethodDict[method] = immune;
    }

    public bool IsImmuneDamageType(DamageType type) {
        if (m_ImmuneDict.ContainsKey(type)) return m_ImmuneDict[type];
        m_ImmuneDict.Add(type, false);
        return false;
    }

    public bool IsImmuneDamageMethod(MethodType method) {
        if (m_ImmuneMethodDict.ContainsKey(method)) return m_ImmuneMethodDict[method];
        m_ImmuneMethodDict.Add(method, false);
        return false;
    }

    public HitResult CalculateDamage(Actor defender, Actor attacker, DamageSource source) {
        if (defender.Health.Invincible) {
            GameCore.Event.Fire(this,
                DamageAfterHitEventArgs.Create(attacker, defender, source, HitResult.InvincibleHitResult));
            return HitResult.InvincibleHitResult;
        }

        var success = true;
        var crit = false;
        var lastHit = false;
        var hurt = false;
        var evade = false;
        var block = false;

        float damage;
        bool immune = IsImmuneDamageType(source.Type) || IsImmuneDamageMethod(source.Method);

        if (immune) {
            GameCore.Event.Fire(this,
                DamageAfterHitEventArgs.Create(attacker, defender, source, HitResult.InvincibleHitResult));
            return HitResult.InvincibleHitResult;
        }

        GameCore.Event.Fire(this, DamageBeforeHitEventArgs.Create(attacker, defender, source));

        switch (source.Type) {
            case DamageType.PHYSICAL:
                damage = CalculatePhysicalDamage(defender, attacker, source, out success, out crit, out hurt, out evade,
                    out block);
                break;
            case DamageType.MAGIC:
                damage = CalculateMagicDamage(defender, attacker, source);
                break;
            case DamageType.RAW:
                damage = CalculateRawDamage(source, defender);
                break;
            default:
                damage = defender.Health.MaxHealth * 0.01f;
                break;
        }

        // Clear temporary mods
        m_AttackerTempMods.Clear(attacker.Stat);
        m_DefenderTempMods.Clear(defender.Stat);


        if (damage > 0f) {
            defender.Health.Damage(damage, source.Type);

            if (!defender.IsDead && defender.Health.CurrentHealth <= 0f) {
                defender.IsDead = true;
                lastHit = true;
            }
        }

        var hitResult = new HitResult(success, false, crit, lastHit, hurt, evade, block, damage, source.Type);

        if (hitResult.Success) {
            if (source.Method != MethodType.REFLECT) {
                //defender.Graphic.FlashColor(0.14f, 1);
            }
        }

        GameCore.Event.Fire(this, DamageAfterHitEventArgs.Create(attacker, defender, source, hitResult));

        if (lastHit) {
            GameCore.Event.Fire(this, DamageLastHitEventArgs.Create(attacker, defender, source, hitResult));
        }

        return hitResult;
    }

    private float CalculateRawDamage(DamageSource source, Actor defender) {
        return source.Value + defender.Health.MaxHealth * source.DamageHealthPercentage;
    }

    private float CalculatePhysicalDamage(Actor defender, Actor attacker, DamageSource source, out bool success,
        out bool crit, out bool hurt,
        out bool evade, out bool block) {
        success = true;
        crit = false;
        hurt = false;
        evade = false;
        block = false;

        MethodType methodType = source.Method;

        var def = defender.Stat;
        m_DefenderTempMods.ApplyModifiers(def);

        var atk = attacker.Stat;
        m_AttackerTempMods.ApplyModifiers(atk);

        // Cannot dodge Damage Overtime
        // float evadeChance = def.GetValue(StatKey.Evasion);
        // if (methodType == MethodType.OVERTIME || methodType == MethodType.REFLECT) evadeChance = 0f;

        // Apply evasion
        // if (!source.CannotEvade && Utility.Random.RandomBoolean(evadeChance)) {
        //     success = false;
        //     evade = true;
        //     return 0f;
        // }

        // Calculate base damage
        float damage = source.Value;

        // Apply skill damage
        // if (tagger != null && tagger.HasTag(Tags.Actor_Skill)) {
        //     atk.SetBaseValue(StatKey.SkillDamage, damage);
        //     damage = atk.GetValue(StatKey.SkillDamage);
        // }

        // Aplly random damage range
        // damage *= Random.Range(1f, 1.1f);

        // Reflect damage to attacker. Cannot reflect Reflect and Overtime damage
        if (methodType != MethodType.REFLECT && methodType != MethodType.OVERTIME) {
            float reflectDamage = damage * def.GetValue(StatKey.ReflectDamageMul, 0f);

            if (reflectDamage > 0f) {
                m_ReflectDamageDealer.DamageSource.Value = reflectDamage;
                m_ReflectDamageDealer.DealDamage(defender, attacker);
            }
        }

        // Apply pierce chance. DOT has 100% ignoring armor
        // float piercingChance = atk.GetValue(StatKey.PiercingArmorChance);
        // if (methodType == MethodType.OVERTIME) piercingChance = 1f;

        // bool pierce = Utility.Random.RandomBoolean(piercingChance);
        // float blockAmount = 0f;

        // Apply block chance if not piercing
        // if (!pierce) {
        //     float blockChance = def.GetValue(StatKey.BlockChance);
        //
        //     if (Utility.Random.RandomBoolean(blockChance)) {
        //         success = false;
        //         block = true;
        //         blockAmount = damage * def.GetValue(StatKey.BlockReduction);
        //     }
        // }

        // Apply crit. DOT cannot crit
        // float critChance = atk.GetValue(StatKey.CritChance);
        //
        // if (methodType == MethodType.OVERTIME || methodType == MethodType.REFLECT) critChance = 0f;
        //
        // float critMul = atk.GetValue(StatKey.CritMul, 1.5f);
        // float critDamageReduction = def.GetValue(StatKey.CritDamageReduction, 0f);
        //
        // crit = Utility.Random.RandomBoolean(critChance);
        //
        // if (crit) {
        //     damage = critMul * damage * (1f - critDamageReduction);
        // }

        // Early interrupt if defender dmg receive mul = 0
        // float dmgReceiveMul = 1f - def.GetValue(StatKey.DamageReduction);

        // Attacker stats
        // float armorPen = atk.GetValue(StatKey.ArmorPen);
        // float lifeSteal = atk.GetValue(StatKey.LifeStealPercentage);
        // float hurtChance = atk.GetValue(StatKey.HurtChance);
        // float physicalDamageMul = atk.GetValue(StatKey.PhysicalDamageReduction, 1f);
        // float damageAgainstBossMul = atk.GetValue(StatKey.DamageAgainstBossMul, 1f);
        // float damageAgainstEliteMul = atk.GetValue(StatKey.DamageAgainstEliteMul, 1f);
        // float damageAgainstMiniEliteMul = atk.GetValue(StatKey.DamageAgainstEliteMul, 1f);

        // Defender stats
        // float armor = def.GetValue(StatKey.Armor);
        // float bossDamageReduction = def.GetValue(StatKey.BossDamageReduction, 0f);
        // float eliteDamageReduction = def.GetValue(StatKey.EliteDamageReduction, 0f);
        // float miniEliteDamageReduction = def.GetValue(StatKey.MiniEliteDamageReduction, 0f);
        // float meleeDamageReduction = def.GetValue(StatKey.MeleeDamageReduction, 0f);
        // float rangeDamageReduction = def.GetValue(StatKey.RangeDamageReduction, 0f);

        // if (methodType == MethodType.OVERTIME) {
        //     dmgReceiveMul = 1f;
        //     armor = 0f;
        //     lifeSteal = 0f;
        //     meleeDamageReduction = 0f;
        //     rangeDamageReduction = 0f;
        //     bossDamageReduction = 0f;
        //     eliteDamageReduction = 0f;
        //     miniEliteDamageReduction = 0f;
        // }

        // Apply armor penetration
        // armor = Mathf.Clamp((1f - armorPen) * armor, 0f, armor);
        // damage -= blockAmount;

        // float damageReduction = armor / (armor + 10f * damage);
        // damage *= (1f - damageReduction);

        // switch (methodType) {
        //     // Apply melee damage reduction
        //     case MethodType.MELEE:
        //         damage *= (1f - meleeDamageReduction);
        //         break;
        //     // Apply range damage reduction
        //     case MethodType.RANGE:
        //         damage *= (1f - rangeDamageReduction);
        //         break;
        // }

        // Apply boss mitigate damage modifier
        // if (attacker.Tagger.HasTag(Tags.Actor_Boss)) {
        //     damage *= (1f - bossDamageReduction);
        // }

        // Apply elite mitigate damage mod
        // if (attacker.Tagger.HasTag(Tags.Actor_Elite)) {
        //     damage *= (1f - eliteDamageReduction);
        // }

        // Apply mini elite mitigate damage mod
        // if (attacker.Tagger.HasTag(Tags.Actor_MiniElite)) {
        //     damage *= (1f - miniEliteDamageReduction);
        // }

        // Apply boss damage modifier
        // if (defender.Tagger.HasTag(Tags.Actor_Boss)) {
        //     damage *= damageAgainstBossMul;
        // }

        // Apply elite damage mod
        // if (defender.Tagger.HasTag(Tags.Actor_Elite)) {
        //     damage *= damageAgainstEliteMul;
        // }

        // if (defender.Tagger.HasTag(Tags.Actor_MiniElite)) {
        //     damage *= damageAgainstMiniEliteMul;
        // }

        // monster damage modifier
        // if (atk.HasStat(StatKey.DmgAgainstMonster) && defender.Tagger.HasTag(Tags.Actor_Monster)) {
        //     damage *= atk.GetValue(StatKey.DmgAgainstMonster);
        // }

        // monster damage modifier
        // if (def.HasStat(StatKey.ExtraMonsterDamageReduction) && attacker.Tagger.HasTag(Tags.Actor_Monster)) {
        //     damage *= 1f - def.GetValue(StatKey.ExtraMonsterDamageReduction);
        // }

        // player damage modifier
        // if (atk.HasStat(StatKey.PlayerDamageMul) && defender.Tagger.HasTag(Tags.Player)) {
        //     damage *= atk.GetValue(StatKey.PlayerDamageMul);
        // }

        // player damage modifier
        // if (def.HasStat(StatKey.ExtraPlayerDamageReduction) && attacker.Tagger.HasTag(Tags.Player)) {
        //     damage *= 1f - def.GetValue(StatKey.ExtraPlayerDamageReduction);
        // }

        // Apply backstab
//            if (attacker.MovementEngine.IsBehindOf(defender.MovementEngine)) damage *= backstabMul;

        // Apply physical damage mul
        // damage *= physicalDamageMul;

        // Apply damage receive mul
        // damage *= dmgReceiveMul;

        // Apply life steal
        // float life = damage * lifeSteal;
        // attacker.Health.Healing(life);

        // Apply hurt
        // if (!defender.IsHurt && !source.CannotHurt) {
        //     hurt = true;
        //     defender.IsHurt = Utility.Random.RandomBoolean(hurtChance);
        //
        //     if (defender.IsHurt) {
        //         Messenger.Broadcast(EventKey.Hurt, defender.gameObject.GetInstanceID(), Vector3.zero);
        //     }
        // }

        damage += defender.Health != null ? defender.Health.MaxHealth * source.DamageHealthPercentage : 0f;
        if (damage < MinDamage) damage = MinDamage;
        return damage;
    }

    private float CalculateMagicDamage(Actor defender, Actor attacker, DamageSource source) {
        var def = defender.Stat;
        var atk = attacker.Stat;
        m_DefenderTempMods.ApplyModifiers(def);

        // float magicArmor = def.GetValue(StatKey.MagicArmor);

        // var dmgReceiveMul = 1f - def.GetValue(StatKey.DamageReduction);

        // if (dmgReceiveMul <= 0f) return 0f;

        // float bossDamageReceiveMul = 1f;
        // float magicPen = 0f;
        // float magicalDamageMul = 1f;
        // float lifeSteal = 0f;

        m_AttackerTempMods.ApplyModifiers(atk);

        // float hurtChance = atk.GetValue(StatKey.HurtChance);
        //
        // if (atk.HasStat(StatKey.MagicPen)) magicPen = atk.GetValue(StatKey.MagicPen);
        //
        // if (atk.HasStat(StatKey.MagicalDamageMul)) magicalDamageMul = atk.GetValue(StatKey.MagicalDamageMul);
        //
        // if (atk.HasStat(StatKey.LifeStealPercentage)) lifeSteal = atk.GetValue(StatKey.LifeStealPercentage);
        //
        // if (def.HasStat(StatKey.BossDamageReduction)) bossDamageReceiveMul = def.GetValue(StatKey.BossDamageReduction);

        float damage = source.Value;

        // Aplly random damage range
        // damage *= Random.Range(1f, 1.1f);

        // Apply armor
        // damage = damage * Mathf.Clamp01((1 - magicArmor * magicPen));

        // Apply magical damage mul
        // damage *= magicalDamageMul;

        // Apply boss damage receive mul
        // damage *= bossDamageReceiveMul;

        // Apply damage receive mul
        // damage *= dmgReceiveMul;

        // monster damage modifier
        // if (atk.HasStat(StatKey.DmgAgainstMonster) && defender.Tagger.HasTag(Tags.Actor_Monster))
        // {
        //     damage *= def.GetValue(StatKey.DmgAgainstMonster);
        // }

        // monster damage modifier
        // if (def.HasStat(StatKey.ExtraMonsterDamageReduction) && attacker.Tagger.HasTag(Tags.Actor_Monster))
        // {
        //     damage *= 1f - def.GetValue(StatKey.ExtraMonsterDamageReduction);
        // }

        // player damage modifier
        // if (atk.HasStat(StatKey.PlayerDamageMul) && defender.Tagger.HasTag(Tags.Player))
        // {
        //     damage *= def.GetValue(StatKey.PlayerDamageMul);
        // }
        // player damage modifier
        // if (def.HasStat(StatKey.ExtraPlayerDamageReduction) && attacker.Tagger.HasTag(Tags.Player))
        // {
        //     damage *= 1f - def.GetValue(StatKey.ExtraPlayerDamageReduction);
        // }

        // Apply hurt
        // if (!defender.IsHurt && !source.CannotHurt)
        // {
        //     defender.IsHurt = Utility.Random.RandomBoolean(hurtChance);
        //
        //     if (defender.IsHurt) Messenger.Broadcast(EventKey.Hurt, defender.gameObject.GetInstanceID(), Vector3.zero);
        // }

        // Apply life steal
        // if (attacker != null)
        // {
        //     float life = damage * lifeSteal;
        //     if (life > 0f) Messenger.Broadcast(EventKey.HEALTH_CHANGE, attacker.gameObject.GetInstanceID(), life);
        // }

        // Apply hurt when defender not being hurt
        // if (!defender.IsHurt && !source.CannotHurt)
        // {
        //     defender.IsHurt = Utility.Random.RandomBoolean(hurtChance);
        //
        //     if (defender.IsHurt) Messenger.Broadcast(EventKey.Hurt, defender.gameObject.GetInstanceID(), Vector3.zero);
        // }

        damage += defender.Health != null ? defender.Health.MaxHealth * source.DamageHealthPercentage : 0f;
        if (damage < MinDamage) damage = MinDamage;
        return damage;
    }

    public void AddAttackerTemporaryModifier(StatKey modName, StatModifier mod) {
        m_AttackerTempMods.AddModifier(modName, mod);
    }

    public void AddDefenderTemporaryModifier(StatKey modName, StatModifier mod) {
        m_DefenderTempMods.AddModifier(modName, mod);
    }

    #region Private classes

    private class TemporaryModifier {
        private readonly Dictionary<StatKey, StatModifier> m_TempDict;

        public TemporaryModifier() {
            m_TempDict = new Dictionary<StatKey, StatModifier>();
        }

        public void AddModifier(StatKey name, StatModifier mod) {
            if (!m_TempDict.ContainsKey(name)) {
                m_TempDict.Add(name, mod);
            }
        }

        public void ApplyModifiers(IStatGroup stat) {
            foreach (var key in m_TempDict.Keys) {
                stat.AddModifier(key, m_TempDict[key], this);
            }
        }

        public void Clear(IStatGroup stat) {
            stat.RemoveModifiersFromSource(this);
            m_TempDict.Clear();
        }
    }

    #endregion
}