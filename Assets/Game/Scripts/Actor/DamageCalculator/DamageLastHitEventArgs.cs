using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;
using Game.Core;

public class DamageLastHitEventArgs : BaseEventArgs<DamageLastHitEventArgs> {
    public Actor Attacker;
    public Actor Defender;
    public DamageSource DamageSource;
    public HitResult HitResult;

    public DamageLastHitEventArgs() {
        Attacker = null;
        Defender = null;
        DamageSource = null;
        HitResult = HitResult.FailedResult;
    }

    public static DamageLastHitEventArgs Create(Actor _attacker, Actor _defender, DamageSource _source, HitResult _hitResult) {
        var eventArgs = new DamageLastHitEventArgs();
        eventArgs.Attacker = _attacker;
        eventArgs.Defender = _defender;
        eventArgs.DamageSource = _source;
        eventArgs.HitResult = _hitResult;
        return eventArgs;
    }
}