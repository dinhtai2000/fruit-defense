using System.Collections;
using System.Collections.Generic;
using Core;
using Game.Core;
using UnityEngine;

public class DamageBeforeHitEventArgs : BaseEventArgs<DamageBeforeHitEventArgs> {
    public Actor Attacker;
    public Actor Defender;
    public DamageSource DamageSource;

    public DamageBeforeHitEventArgs() {
        Attacker = null;
        Defender = null;
        DamageSource = null;
    }

    public static DamageBeforeHitEventArgs Create(Actor _attacker, Actor _defender, DamageSource _source) {
        var eventArgs = new DamageBeforeHitEventArgs();
        eventArgs.Attacker = _attacker;
        eventArgs.Defender = _defender;
        eventArgs.DamageSource = _source;
        return eventArgs;
    }
}