using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

public interface IDamageCalculator {
    Actor Owner { get; }
    void Init(Actor actor);
    void SetImmuneDamageType(DamageType type, bool immune);
    void SetImmuneDamageMethod(MethodType method, bool immune);
    void AddAttackerTemporaryModifier(StatKey modName, StatModifier mod);
    void AddDefenderTemporaryModifier(StatKey modName, StatModifier mod);
    HitResult CalculateDamage(Actor defender, Actor attacker, DamageSource source);
}