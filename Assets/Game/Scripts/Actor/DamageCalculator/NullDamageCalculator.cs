using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;

public class NullDamageCalculator : IDamageCalculator {
    public Actor Owner { get; private set; }

    public void Init(Actor actor) {
        Owner = actor;
    }

    public void SetImmuneDamageType(DamageType type, bool immune) {
    }

    public void SetImmuneDamageMethod(MethodType method, bool immune) {
    }

    public void AddAttackerTemporaryModifier(StatKey modName, StatModifier mod) {
    }

    public void AddDefenderTemporaryModifier(StatKey modName, StatModifier mod) {
    }

    public HitResult CalculateDamage(Actor defender, Actor attacker, DamageSource source) {
        return HitResult.FailedResult;
    }
}