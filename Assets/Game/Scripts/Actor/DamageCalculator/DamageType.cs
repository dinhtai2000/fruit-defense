using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType {
    PHYSICAL = 0,
    MAGIC = 1,
    RAW = 2
}