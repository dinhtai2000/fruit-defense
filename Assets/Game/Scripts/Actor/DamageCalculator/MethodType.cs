using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MethodType {
    NONE = 0,
    MELEE = 1,
    RANGE = 2,
    AOE = 3,
    OVERTIME = 4,
    REFLECT = 5
}