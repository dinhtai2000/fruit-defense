using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITargetQuery {
    void Init(ITargetFinder finder);
    Actor GetTarget();
    void SetTarget(Actor target);
    void ForceUpdateTarget();
    void OnUpdate();
}