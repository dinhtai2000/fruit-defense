using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullTargetQuery : ITargetQuery {
    public void Init(ITargetFinder finder) {
    }

    public Actor GetTarget() {
        return null;
    }

    public void SetTarget(Actor target) {
    }

    public void ForceUpdateTarget() {
    }

    public void OnUpdate() {
    }
}