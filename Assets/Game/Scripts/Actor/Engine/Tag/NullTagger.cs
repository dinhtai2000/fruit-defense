﻿using System.Collections.Generic;

public class NullTagger : ITagger
{
    public void AddTag(string tag)
    {
    }

    public void AddTags(IEnumerable<string> tags)
    {
    }

    public bool HasAnyTags(IEnumerable<string> tags)
    {
        return false;
    }

    public bool HasAnyTags(params string[] tags)
    {
        return false;
    }

    public bool HasTag(string tag)
    {
        return false;
    }

    public bool HasTags(IEnumerable<string> tags)
    {
        return false;
    }

    public bool HasTags(params string[] tags)
    {
        return false;
    }
}
