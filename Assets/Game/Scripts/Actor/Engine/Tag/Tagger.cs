﻿using System.Collections.Generic;
using UnityEngine;

public class Tagger : MonoBehaviour, ITagger
{
    public List<string> tags = new List<string>();

    public void AddTag(string tag)
    {
        if (HasTag(tag)) return;
        tags.Add(tag);
    }

    public void AddTags(IEnumerable<string> tags)
    {
        foreach (var tag in tags)
        {
            AddTag(tag);
        }
    }

    public bool HasAnyTags(IEnumerable<string> tags)
    {
        foreach (var tag in tags)
        {
            if (HasTag(tag)) return true;
        }
        return false;
    }

    public bool HasAnyTags(params string[] tags)
    {
        foreach (string tag in tags)
        {
            if (HasTag(tag)) return true;
        }
        return false;
    }

    public bool HasTag(string tag)
    {
        return tags.Contains(tag);
    }

    public bool HasTags(IEnumerable<string> tags)
    {
        foreach (string tag in tags)
        {
            if (!HasTag(tag)) return false;
        }
        return true;
    }

    public bool HasTags(params string[] tags)
    {
        foreach (string tag in tags)
        {
            if (!HasTag(tag)) return false;
        }
        return true;
    }
}