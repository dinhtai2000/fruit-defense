﻿using System.Collections.Generic;

/// <summary>
/// Tag object => object is Object|Enemy|Fly|Greate...
/// </summary>
public interface ITagger
{
	bool HasTag(string tag);
	void AddTag(string tag);
	void AddTags(IEnumerable<string> tags);
	bool HasTags(IEnumerable<string> tags);
	bool HasTags(params string[] tags);
	bool HasAnyTags(IEnumerable<string> tags);
    bool HasAnyTags(params string[] tags);
}