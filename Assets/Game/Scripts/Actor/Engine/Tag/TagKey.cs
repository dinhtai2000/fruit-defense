﻿public static class TagKey
{
    public static string Ally = nameof(Ally);
    public static string Enemy = nameof(Enemy);
    public static string Melee = nameof(Melee);
    public static string Range = nameof(Range);
}