using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class StatusFollowBone : StatusPlacement {
    [SerializeField] private string m_BoneName;

    private Transform m_Trans;
    private Bone m_Bone;
    private Actor m_Target;

    public override void Place(Transform trans, Actor actor) {
        m_Trans = trans;
        m_Target = actor;
        m_Bone = m_Target.Animation.FindBone(m_BoneName);
    }

    private void Update() {
        if (m_Bone != null) {
            m_Trans.position = m_Bone.GetWorldPosition(m_Target.Trans);
        }
    }
}