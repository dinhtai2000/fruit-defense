using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusAtCenter : StatusPlacement {
    [SerializeField] private Vector3 m_Offset;

    public override void Place(Transform trans, Actor actor) {
        var position = actor.CenterTransform.localPosition;
        position += m_Offset;
        trans.localPosition = position;
    }
}