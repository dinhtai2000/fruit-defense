using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusOnHead : StatusPlacement {
    [SerializeField] private Vector3 m_Offset;

    public override void Place(Transform trans, Actor actor) {
        var position = actor.TopPosition;
        position += m_Offset;
        trans.position = position;
    }
}