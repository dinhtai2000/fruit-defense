using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusPlacement : MonoBehaviour {
    public abstract void Place(Transform trans, Actor actor);
}