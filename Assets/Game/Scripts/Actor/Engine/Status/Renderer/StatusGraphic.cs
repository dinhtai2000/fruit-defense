using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusGraphic : MonoBehaviour {
    [SerializeField] private Sprite m_Icon;
    public Sprite Icon => m_Icon;
}