using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invincible : BaseStatus {
    private Actor actor;

    public override void Begin() {
        base.Begin();
        actor = GetComponentInParent<Actor>();
        if (actor != null) {
            actor.Health.Invincible = true;
            actor.SetActiveCollider(false);
        }
    }

    public override void Execute() {
        base.Execute();
        if (actor != null) {
            actor.Health.Invincible = true;
            actor.SetActiveCollider(false);
        }
    }

    public override void End() {
        base.End();
        if (actor != null) {
            actor.Health.Invincible = false;
            actor.SetActiveCollider(true);
        }
    }
}