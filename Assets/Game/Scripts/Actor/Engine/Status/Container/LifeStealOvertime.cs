using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeStealOvertime : BaseStatus {
    [SerializeField, Range(0f, 1f)] private float m_LifeStealPercentagePerSec;
    [SerializeField] private float m_LifeStealPerSec;
    [SerializeField, Range(0f, 10f)] private float m_LifeStealInterval = 1f;

    private float m_Timer;
    private float m_StealHp;

    private void OnEnable() {
    }

    private void OnDisable() {
    }

    public override void Begin() {
        base.Begin();
        m_Timer = 0f;
        m_StealHp = Actor.Health.MaxHealth * m_LifeStealPercentagePerSec + m_LifeStealPerSec;
    }

    public override void Execute() {
        base.Execute();
        if (Actor == null) return;
        m_Timer += Time.deltaTime;

        if (m_Timer >= m_LifeStealInterval) {
            m_Timer = 0f;
            Actor.Health.Healing(-m_StealHp);
        }
    }
}