using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
public class StatStatus : BaseStatus {
    [SerializeField, BoxGroup("Stat"), HideLabel, InlineProperty, GUIColor(0.3f, 0.8f, 0.8f)]
    private ModifierData m_ModifierData;

    public ModifierData ModifierData => m_ModifierData;

    protected override void Awake() {
        base.Awake();
        m_ModifierData.Modifier.Source = this;
    }

    public override IStatus SetModifierValue(float value) {
        m_ModifierData.Modifier.Value = value;
        return base.SetModifierValue(value);
    }

    public override void Begin() {
        base.Begin();
        if (Actor != null) {
            Actor.Stat.AddModifier(m_ModifierData.AttributeName, m_ModifierData.Modifier, this);
        }
    }

    public override void End() {
        base.End();
        if (Actor != null) {
            Actor.Stat.RemoveModifiersFromSource(this);
        }
    }
}