using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Knockback : BaseStatus {
    [SerializeField] private float m_KnockbackValue;

    private Vector3 m_Dir;
    private Tween m_Tween;

    public float KnockbackValue {
        get => m_KnockbackValue;
        set => m_KnockbackValue = value;
    }

    public override void Begin() {
        base.Begin();
        Actor.Movement.LockMovement = true;
        m_Tween = AnimateMovement();
        m_Tween.onComplete = Stop;
    }

    public override void End() {
        base.End();
        m_Tween?.Kill();
        if (!Actor.IsDead) {
            Actor.Movement.LockMovement = false;
        }
    }

    public override void Execute() {
        base.Execute();
        if (Actor.IsDead) {
            Stop();
        }
    }

    private Tween AnimateMovement() {
        m_Dir = Vector3.Normalize(Source.Trans.position - Actor.Trans.position);
        m_Dir.y = 0;
        m_Dir.z = 0;

        var dest = Actor.Trans.position - (m_Dir * m_KnockbackValue);
        return Actor.Movement.MoveTween(dest, Duration.Value).SetEase(Ease.OutQuad).Play();
    }
}