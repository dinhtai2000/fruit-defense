using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAtCenter : StatusScaler {
    [SerializeField] private Vector3 m_Offset;

    public override void Scale(Transform trans, Actor actor) {
        var scale = new Vector3(actor.Height / 2f, actor.Height / 2f);
        scale += m_Offset;
        trans.localScale = scale;
    }
}