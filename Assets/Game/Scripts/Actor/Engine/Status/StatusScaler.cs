using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusScaler : MonoBehaviour {
    public abstract void Scale(Transform trans, Actor actor);
}