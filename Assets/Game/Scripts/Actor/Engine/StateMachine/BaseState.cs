﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Fsm
{
    public abstract class BaseState : MonoBehaviour, IState
    {
        public Actor Actor { set; get; }
        public virtual void Enter() 
        {
            Debug.Log("Enter " + name + " State on " + Actor.name);
        }
        public virtual void Execute() { }
        public virtual void Exit() { }
        public virtual void Reset() { }
        public virtual void InitializeStateMachine() { }
    }
}