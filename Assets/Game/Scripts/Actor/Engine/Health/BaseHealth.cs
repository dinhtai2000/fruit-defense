using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class BaseHealth : MonoBehaviour, IHealth {
    public Actor Owner { get; private set; }
    public event Action<IHealth> OnValueChanged;
    [ShowInInspector]
    public bool Initialized { get; set; }
    public abstract float CurrentHealth { get; set; }
    public abstract float MaxHealth { get; set; }
    public abstract float MinHealth { get; set; }
    public abstract float HealthPercentage { get; }
    public abstract bool Invincible { get; set; }

    public abstract void Damage(float damage, DamageType type);

    public abstract void Healing(float amount);

    public abstract void Reset();

    private Dictionary<DamageType, float> m_ShieldDict = new Dictionary<DamageType, float>();
    private List<Action<float, DamageType>> m_OnRecieveDamage = new List<Action<float, DamageType>>();

    [Button]
    public void Kill() {
        CurrentHealth = 0;
    }

    public void Init(Actor actor) {
        Owner = actor;
        var damageTypes = Enum.GetValues(typeof(DamageType)) as DamageType[];
        if (damageTypes != null) {
            foreach (var t in damageTypes) {
                m_ShieldDict.Add(t, 0f);
            }
        }
    }

    public void SubscribeReceiveDamageEvent(Action<float, DamageType> callback) {
        m_OnRecieveDamage.Add(callback);
    }

    public void AddShieldDamageType(DamageType damageType, float amount) {
        m_ShieldDict[damageType] += amount;
    }

    public void SetShieldDamageType(DamageType damageType, float amount) {
        m_ShieldDict[damageType] = amount;
    }

    public float GetShieldDamageType(DamageType damageType) {
        return m_ShieldDict[damageType];
    }

    protected void InvokeChangeValue() {
        OnValueChanged?.Invoke(this);
    }

    protected float DamageShieldDamageType(DamageType damageType, float damage) {
        if (m_ShieldDict == null || !m_ShieldDict.ContainsKey(damageType)) {
            return damage;
        }
        
        var currentShield = m_ShieldDict[damageType];

        if (currentShield <= 0f) {
            return damage;
        }

        if (currentShield > damage) {
            currentShield -= damage;
            m_ShieldDict[damageType] = currentShield;
            return 0f;
        }

        var remainingDamage = damage - currentShield;
        currentShield = 0f;
        m_ShieldDict[damageType] = currentShield;
        return remainingDamage;
    }

    protected void InvokeDamageEvent(float damage, DamageType type) {
        foreach (var callback in m_OnRecieveDamage) {
            callback.Invoke(damage, type);
        }
    }
}