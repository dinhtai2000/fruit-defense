﻿namespace Game.Skill
{
    public interface ISkill
    {
        Actor Caster { get; }
        int Id { get; }
        bool CanCast { get; }
        bool IsExecuting { get; }
        bool IsCoolingDown { get; }
        void SetCoolDown(float time);
        float GetCoolDown();
        void Initialize(Actor actor);
        void Ticks();
        void Cast();
        void Stop();
        void AddModifierCooldown(StatModifier modifier);
        void RemoveModifierCooldown(StatModifier modifier);
        void SetCooldown(Stat cooldown);
    }
}