﻿public interface IStopHandler
{
    void OnStop();
}