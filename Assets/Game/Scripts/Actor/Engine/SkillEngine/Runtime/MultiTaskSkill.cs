﻿using Game.Tasks;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Game.Skill
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TaskRunner))]
    public class MultiTaskSkill : BaseSkill
    {
        [SerializeField] private TaskRunner _taskRunner;
        public Task CurrentTask { get { return _taskRunner.CurrentTask; } }
        public Task[] Tasks { get { return _taskRunner.Tasks; } }
        public BaseSkill skillConnectCooldownWhenSkillCompleted;
        private void OnValidate()
        {
            _taskRunner = GetComponent<TaskRunner>();
        }
        protected override void Start()
        {
            base.Start();
        }
        public override void Cast()
        {
            totalCast++;
            base.Cast();
            _taskRunner?.RunTask();
            _taskRunner.OnComplete -= OnCompleteSkill;
            _taskRunner.OnComplete += OnCompleteSkill;
        }

        public override void Stop()
        {
            _taskRunner?.StopTask();
            _taskRunner.OnComplete -= OnCompleteSkill;
        }
        private void OnCompleteSkill()
        {
            _taskRunner.OnComplete -= OnCompleteSkill;
            IsExecuting = false;
            StartCooldown();
            skillConnectCooldownWhenSkillCompleted?.StartCooldown();
        }
    }
}