﻿using Cysharp.Threading.Tasks;
using Game.Skill;
using Game.Tasks;

public class SkillTask : Task, IStopHandler
{
    private BaseSkill _skill;
    public BaseSkill Skill => _skill;
    public Actor Caster { get; set; }
    public override void UnityAwake()
    {
        base.UnityAwake();
        Caster = GetComponentInParent<Actor>();
        if (_skill == null)
            _skill = GetComponentInParent<BaseSkill>();
    }
    public override void End()
    {
        //if (Skill)
        //{
        //    Skill.StartCooldown();
        //}
        base.End();
    }

    public virtual void OnStop()
    {
        IsCompleted = true;
    }
}
//[System.Serializable]
//public class SequenceTask 
//{
//    private BaseSkill _skill;
//    public BaseSkill Skill => _skill;
//    public ActorBase Caster { get; set; }
//    public override void UnityAwake()
//    {
//        base.UnityAwake();
//        Caster = GetComponentInParent<ActorBase>();
//        if (_skill == null)
//            _skill = GetComponentInParent<BaseSkill>();
//    }
//    public bool IsRunning { set; get; }
//    public bool ForceInterruptTask { set; get; }

//    public bool IsCompleted { set; get; } = false;

//    public virtual void UnityEnable() { }
//    public virtual void UnityDisable() { }
//    public virtual void UnityAwake() { }
//    public virtual void UnityStart() { }

//    public async void BeginEvent()
//    {
//        await Begin();
//    }

//    public virtual async UniTask Begin()
//    {
//        IsCompleted = false;
//        IsRunning = true;
//        await UniTask.Yield();
//    }

//    public virtual async UniTask End()
//    {
//        IsCompleted = true;
//        IsRunning = false;
//        await UniTask.Yield();
//    }

//    public virtual void Run()
//    {
//        if (IsCompleted || !IsRunning) return;
//        if (_completeWhenAllChildrenStop)
//        {
//            bool childrenComplete = true;
//            foreach (var parallelTask in _parallelTasks)
//            {
//                if (parallelTask.GetInstanceID() != GetInstanceID() && !parallelTask.IsCompleted)
//                {
//                    childrenComplete = false;
//                    break;
//                }
//            }

//            if (childrenComplete)
//                IsCompleted = true;
//        }
//    }
//}