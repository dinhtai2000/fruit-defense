using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public interface IMovement {
    float Speed { set; get; }
    float DirectionSign { get; }
    bool LockMovement { set; get; }
    bool IsMoving { set; get; }
    bool UsingHorizontalBound { set; get; }
    bool UsingVerticalBound { set; get; }
    bool ReachBoundLeft { get; }
    bool ReachBoundRight { get; }
    bool ReachBoundTop { get; }
    bool ReachBoundBottom { get; }
    bool ReachBound { get; }
    Vector3 CurrentDirection { get; }
    Vector3 CurrentPosition { get; }
    Vector3 FacingDirection { get; }
    Bound2D MovementBound { get; }

    void Init(Actor actor);

    void Teleport(Vector3 pos);

    void SyncGraphicRotation(Vector3 dir);

    void FlipFacingDirection();

    void LookAt(Vector3 position);

    Vector3 Bound(Vector3 position);

    void Bound();

    Tween MoveTween(Vector3 position, float duration, System.Action onComplete = null, Ease easeType = Ease.Linear, bool isMoveSpeed = false,
        bool syncGraphicDirection = false);

    bool MoveTo(Vector3 position);

    bool MoveDirection(Vector3 direction);

    void OnUpdate();

    bool MoveTo(Vector3 position, float speed);

    bool MoveDirection(Vector3 direction, float speed);
}