using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Movement : MonoBehaviour, IMovement {
    [SerializeField] private float m_Speed;
    [SerializeField] private bool m_LockMovement;
    [SerializeField] private bool m_LockFacingDirection;
    [SerializeField] private bool m_HorizontalBound = true;
    [SerializeField] private bool m_VerticalBound = true;
    [SerializeField] private Vector3 m_InitDirection= Vector3.right;
    [SerializeField] private Bound2D m_MovementBound;

    private bool m_IsMoving;
    private Transform m_Trans;
    private Transform m_GraphicTrans;
    private Vector3 m_LastPosition;

    public float Speed {
        get { return m_Speed; }
        set { m_Speed = value; }
    }

    public bool LockMovement {
        get { return m_LockMovement; }
        set { m_LockMovement = value; }
    }

    public bool IsMoving {
        get { return m_IsMoving; }
        set { m_IsMoving = value; }
    }

    public bool UsingHorizontalBound {
        set { m_HorizontalBound = value; }
        get { return m_HorizontalBound; }
    }

    public bool UsingVerticalBound {
        get { return m_VerticalBound; }
        set { m_VerticalBound = value; }
    }

    public bool ReachBoundLeft => m_Trans.position.x <= m_MovementBound.XLow;

    public bool ReachBoundRight => m_Trans.position.x >= m_MovementBound.XHigh;

    public bool ReachBoundTop => m_Trans.position.y >= m_MovementBound.YHigh;

    public bool ReachBoundBottom => m_Trans.position.y <= m_MovementBound.YLow;

    public bool ReachBound => ReachBoundBottom || ReachBoundLeft || ReachBoundRight || ReachBoundTop;

    public Bound2D MovementBound => m_MovementBound;

    public float DirectionSign { private set; get; }

    public Vector3 FacingDirection {
        get { return Vector3.right * DirectionSign; }
    }

    public Vector3 CurrentDirection { private set; get; }
    public Vector3 CurrentPosition { get; }

    public void Init(Actor actor) {
        m_Trans = transform;
        m_GraphicTrans = actor.GraphicTrans;
        SyncGraphicRotation(m_InitDirection);
    }

    public void Teleport(Vector3 pos) {
        m_Trans.position = pos;
    }

    public void SyncGraphicRotation(Vector3 dir) {
        if (m_LockFacingDirection) return;

        DirectionSign = dir.x < 0f ? -1f : 1f;
        CurrentDirection = dir;
        m_GraphicTrans.localRotation = Quaternion.Euler(0f, DirectionSign >= 0f ? 0f : 180f, 0f);
    }

    public void FlipFacingDirection() {
        if (m_LockFacingDirection) return;

        DirectionSign *= -1f;
        m_GraphicTrans.localRotation = Quaternion.Euler(0f, DirectionSign >= 0f ? 0f : 180f, 0f);
    }

    public Vector3 Bound(Vector3 position) {
        if (UsingHorizontalBound) {
            position.x = Mathf.Clamp(position.x, m_MovementBound.XLow, m_MovementBound.XHigh);
        }

        if (UsingVerticalBound) {
            position.y = Mathf.Clamp(position.y, m_MovementBound.YLow, m_MovementBound.YHigh);
        }

        return position;
    }

    public void Bound() {
        Vector3 position = m_Trans.position;

        if (UsingHorizontalBound) {
            position.x = Mathf.Clamp(position.x, m_MovementBound.XLow, m_MovementBound.XHigh);
        }

        if (UsingVerticalBound) {
            position.y = Mathf.Clamp(position.y, m_MovementBound.YLow, m_MovementBound.YHigh);
        }

        m_Trans.position = position;
    }

    public void LookAt(Vector3 position) {
        if (m_LockFacingDirection) return;
        SyncGraphicRotation(position - m_Trans.position);
    }

    public bool MoveTo(Vector3 position) {
        return MoveTo(position, Speed);
    }

    public bool MoveDirection(Vector3 direction) {
        return MoveDirection(direction, Speed);
    }

    public void OnUpdate() {
    }

    public Tween MoveTween(Vector3 position, float duration, Action onComplete = null, Ease easeType = Ease.Linear, bool isMoveSpeed = false,
        bool syncGraphicDirection = false) {
        position.z = 0f;
        if (!IsMoving) IsMoving = true;

        CurrentDirection = Vector3.Normalize(position - m_Trans.position);

        if (syncGraphicDirection) SyncGraphicRotation(CurrentDirection);

        var tween = m_Trans.DOMove(position, duration).SetSpeedBased(isMoveSpeed).SetEase(easeType).OnUpdate(Bound).OnComplete(() => onComplete?.Invoke());
        return tween;
    }

    public bool MoveTo(Vector3 position, float speed) {
        if (LockMovement) return false;

        var curPos = m_Trans.position;
        if (curPos.Compare(position)) return false;
        if (!IsMoving) IsMoving = true;
        //position = Bound(position);
        position.z = 0f;
        m_LastPosition = curPos;
        curPos = Vector3.MoveTowards(curPos, position, Time.deltaTime * speed);
        CurrentDirection = Vector3.Normalize(curPos - m_LastPosition);
        SyncGraphicRotation(CurrentDirection);
        m_Trans.position = curPos;
        return true;
    }

    public bool MoveDirection(Vector3 direction, float speed) {
        if (LockMovement) return false;
        if (direction.Compare(Vector3.zero)) return false;
        if (!IsMoving) IsMoving = true;
        direction.z = 0f;
        CurrentDirection = Vector3.Normalize(direction);
        SyncGraphicRotation(CurrentDirection);
        m_LastPosition = m_Trans.position;
        m_Trans.Translate(Time.deltaTime * speed * direction, Space.World);
        //Bound();
        return true;
    }
}