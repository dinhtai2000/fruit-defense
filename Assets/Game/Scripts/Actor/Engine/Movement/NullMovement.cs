using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class NullMovement : IMovement {
    public float Speed {
        get { return 0f; }
        set { }
    }

    public float DirectionSign {
        get { return -1f; }
    }

    public bool LockMovement {
        get { return true; }
        set { }
    }

    public bool IsMoving {
        get { return false; }
        set { }
    }

    public bool UsingHorizontalBound {
        get { return false; }
        set { }
    }

    public bool UsingVerticalBound {
        get { return false; }
        set { }
    }

    public bool ReachBoundLeft {
        get { return false; }
    }

    public bool ReachBoundRight {
        get { return false; }
    }

    public bool ReachBoundTop {
        get { return false; }
    }

    public bool ReachBoundBottom {
        get { return false; }
    }

    public bool ReachBound {
        get { return false; }
    }

    public Vector3 CurrentDirection {
        get { return Vector3.zero; }
    }

    public Vector3 CurrentPosition { get; }

    public Vector3 FacingDirection {
        get { return Vector3.right; }
    }

    public Bound2D MovementBound {
        get { return Bound2D.zero; }
    }

    public void Init(Actor actor) {
    }

    public void Teleport(Vector3 pos) {
    }

    public void SyncGraphicRotation(Vector3 dir) {
    }

    public void FlipFacingDirection() {
    }

    public void LookAt(Vector3 position) {
    }

    public Vector3 Bound(Vector3 position) {
        return Vector3.zero;
    }

    public void Bound() {
    }

    public Tween MoveTween(Vector3 position, float duration, Action onComplete = null, Ease easeType = Ease.Linear, bool isMoveSpeed = false,
        bool syncGraphicDirection = false) {
        return null;
    }

    public bool MoveTo(Vector3 position) {
        return false;
    }

    public bool MoveDirection(Vector3 direction) {
        return false;
    }

    public void OnUpdate() {
    }

    public bool MoveTo(Vector3 position, float speed) {
        return false;
    }

    public bool MoveDirection(Vector3 direction, float speed) {
        return false;
    }
}