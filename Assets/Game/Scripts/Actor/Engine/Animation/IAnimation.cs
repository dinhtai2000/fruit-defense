using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using AnimationState = Spine.AnimationState;

namespace Core {
    public interface IAnimation {
        Actor Owner { get; }
        bool Lock { set; get; }
        string CurrentAnimationName { get; }
        bool IsCurrentAnimationComplete { get; }
        float FlipX { set; get; }
        float FlipY { set; get; }
        float TimeScale { set; get; }
        void SubscribeEvent(AnimationState.TrackEntryEventDelegate callback);
        void SubscribeComplete(AnimationState.TrackEntryDelegate callback);
        void UnsubcribeEvent(AnimationState.TrackEntryEventDelegate callback);
        void UnsubcribeComplete(AnimationState.TrackEntryDelegate callback);
        bool HasAnimation(string animName);
        bool IsPlaying(string animName);
        void Init(Actor actor);
        Spine.Animation FindAnimation(string animName);
        Spine.EventData FindEvent(string eventName);
        Spine.Bone FindBone(string boneName);
        void Play(string animName, bool loop = true, bool restart = false);
        bool EnsurePlay(string animName, bool loop = true, bool restart = false);
        void Pause();
        void Resume();

        void Stop();

        // void SetAnimationTimeScale(float timeScale);
        void Clear();
        void Play(int track, string animName, bool loop = true, bool restart = false);
        void Add(int track, string animName, bool loop = false, float delay = 0);
    }
}