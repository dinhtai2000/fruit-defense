using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;
using AnimationState = Spine.AnimationState;
using Event = Spine.Event;

namespace Core
{
    public class GroupAnimation : MonoBehaviour, IAnimation
    {
        [SerializeField] private SkeletonAnimation[] m_Animations;

        private float m_FlipX;
        private float m_FlipY;
        private float m_AnimationTimeScale = 1.0f;
        private float m_PrevAnimationTimeScale = 0.0f;
        private bool m_IsAnimationPaused = false;
        private SkeletonAnimation m_Animation;

        public int CountAnimation => m_Animations.Length;
        public Actor Owner { get; private set; }
        public bool Lock { get; set; }

        public string CurrentAnimationName
        {
            get
            {
                var currentTrack = m_Animation.AnimationState.GetCurrent(0);
                return currentTrack != null ? currentTrack.Animation.Name : string.Empty;
            }
        }

        public bool IsCurrentAnimationComplete
        {
            get
            {
                var currentTrack = m_Animation.AnimationState.GetCurrent(0);
                return currentTrack != null && currentTrack.IsComplete;
            }
        }

        public float FlipX
        {
            get { return m_FlipX; }
            set { m_FlipX = value; }
        }

        public float FlipY
        {
            get { return m_FlipY; }
            set { m_FlipY = value; }
        }

        public float TimeScale
        {
            set
            {
                foreach (var anim in m_Animations)
                {
                    anim.timeScale = value;
                }
            }
            get => m_Animation.timeScale;
        }

        public void SubscribeEvent(AnimationState.TrackEntryEventDelegate callback)
        {
            m_Animation.AnimationState.Event += callback;
        }

        public void SubscribeComplete(AnimationState.TrackEntryDelegate callback)
        {
            m_Animation.AnimationState.Complete += callback;
        }

        public void UnsubcribeEvent(AnimationState.TrackEntryEventDelegate callback)
        {
            m_Animation.AnimationState.Event -= callback;
        }

        public void UnsubcribeComplete(AnimationState.TrackEntryDelegate callback)
        {
            m_Animation.AnimationState.Complete -= callback;
        }

        public bool HasAnimation(string animName)
        {
            if (m_Animation == null || m_Animation.Skeleton == null) return false;
            for (int i = 0; i < m_Animation.Skeleton.Data.Animations.Count; i++)
            {
                if (animName == m_Animation.Skeleton.Data.Animations.Items[i].Name)
                {
                    return true;
                }
            }

            Debug.Log("Animation was not found - " + animName);
            return false;
        }

        public bool IsPlaying(string animName)
        {
            if (m_Animation == null) return false;
            if (m_Animation.state?.GetCurrent(0) == null) return false;
            return string.Compare(m_Animation.state.GetCurrent(0).Animation.Name, animName,
                StringComparison.Ordinal) == 0;
        }

        public void Init(Actor actor)
        {
            Owner = actor;
            if (m_Animations.Length > 0)
            {
                m_Animation = m_Animations[0];
            }
        }

        public Spine.Animation FindAnimation(string animName)
        {
            return m_Animation.SkeletonDataAsset.GetSkeletonData(false).FindAnimation(animName);
        }

        public EventData FindEvent(string eventName)
        {
            return m_Animation.Skeleton.Data.FindEvent(eventName);
        }

        public Bone FindBone(string boneName)
        {
            return m_Animation.Skeleton.FindBone(boneName);
        }

        public void Play(string animName, bool loop = true, bool restart = false)
        {
            if (Lock) return;
            if (m_Animation.AnimationState == null || !HasAnimation(animName)) return;
            if (m_Animation.AnimationState == null || (restart == false && IsPlaying(animName))) return;
            foreach (var anim in m_Animations)
            {
                anim.AnimationState.SetAnimation(0, animName, loop);
            }
        }

        public bool EnsurePlay(string animName, bool loop = true, bool restart = false)
        {
            if (Lock) return false;
            if (m_Animation.AnimationState == null || !HasAnimation(animName)) return false;
            if (!restart && IsPlaying(animName)) return true;
            foreach (var anim in m_Animations)
            {
                anim.AnimationState.SetAnimation(0, animName, loop);
            }

            return false;
        }

        public void Pause()
        {
            if (!m_IsAnimationPaused)
            {
                m_IsAnimationPaused = true;
                m_PrevAnimationTimeScale = m_AnimationTimeScale;
                m_AnimationTimeScale = 0.0f;
            }
        }

        public void Resume()
        {
            if (m_IsAnimationPaused)
            {
                m_IsAnimationPaused = false;
                m_AnimationTimeScale = m_PrevAnimationTimeScale;
            }
        }

        public void Stop()
        {
            foreach (var anim in m_Animations)
            {
                anim.state.ClearTracks();
                anim.Skeleton.SetToSetupPose();
            }
        }

        public void SetAnimationTimeScale(float timeScale)
        {
            m_AnimationTimeScale = timeScale;
            m_IsAnimationPaused = false;
        }

        public void Clear()
        {
        }

        public void Play(int track, string animName, bool loop = true, bool restart = false)
        {
            if (Lock) return;
            if (m_Animation.AnimationState == null || !HasAnimation(animName)) return;
            if (m_Animation.AnimationState == null || (restart == false && IsPlaying(animName))) return;
            foreach (var anim in m_Animations)
            {
                anim.AnimationState.SetAnimation(track, animName, loop);
            }
        }

        public void Add(int track, string animName, bool loop = false, float delay = 0)
        {
            if (Lock) return;
            if (m_Animation.AnimationState == null || !HasAnimation(animName)) return;
            if (m_Animation.AnimationState == null) return;
            foreach (var anim in m_Animations)
            {
                anim.AnimationState.AddAnimation(track, animName, loop, delay);
            }
        }
    }
}