﻿using System;

public class ActorFactory : MonoSingleton<ActorFactory>
{

    public Actor Bind(string characterId, Actor actor)
    {
        var entity = DataManager.Base.Character.Get(characterId);
        return Bind(entity, actor);
    }
    public Actor ApplyStat(string characterId, Actor actor, int level)
    {
        var entity = DataManager.Base.Character.Get(characterId);
        return ApplyStat(entity, actor, level); 
    }

    public Actor Bind(CharacterEntity entity, Actor actor)
    {
        entity.Bind(actor.Stat);
        actor.Init();

        foreach (var tag in entity.Tag)
        {
            actor.Tagger.AddTag(tag);
        }
     
        return actor;
    }

    public Actor ApplyStat(CharacterEntity entity, Actor actor, int level)
    {
        actor.Stat.AddModifier(StatKey.Hp, new StatModifier(EStatMod.Flat, entity.iHp * level), this);
        actor.Stat.AddModifier(StatKey.Damage, new StatModifier(EStatMod.Flat, entity.iDmg * level), this);
        actor.Stat.AddModifier(StatKey.AttackSpeed, new StatModifier(EStatMod.Flat, entity.iAtkSpeed * level), this);
        actor.Stat.AddModifier(StatKey.MovementSpeed, new StatModifier(EStatMod.Flat, entity.iSpeed * level), this);
        actor.Stat.AddModifier(StatKey.AttackRange, new StatModifier(EStatMod.Flat, entity.iRange * level), this);
        actor.Stat.AddStat(StatKey.Level, level);
        return actor;
    }
}