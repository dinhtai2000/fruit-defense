using Core;
using Game.Fsm;
using Game.Skill;
using Sirenix.OdinInspector;
using Spine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Actor : MonoBehaviour
{
    public float AttackDamage = 20;
    public float Hp = 20;
    public bool IsActived = false;

    [SerializeField] private LayerMask m_EnemyLayerMask;
    [SerializeField] private LayerMask m_AllyLayerMask;
    [SerializeField] private bool m_AI;
    [SerializeField] private bool m_IsDead;
    [SerializeField] private Transform m_GraphicTrans;
    [SerializeField] private Transform m_HealthTrans;
    [SerializeField] private Rigidbody2D m_RigidBody;
    [SerializeField] private Collider2D m_Collider;

    [SerializeField] private float m_Width = 1f;
    [SerializeField] private float m_Height = 1f;

    private Transform m_Trans;
    private Transform m_CenterTrans;

    private IStatGroup stats;
    private IFsm fsm;
    private ISkillEngine skill;
    private ITagger tagger;
    private ICharacterStatus status;
    private ITargetFinder targetFinder;
    private IHealth health;
    private IMovement movement;
    private IAnimation m_Animation;
    private IInput m_Input;
    private IDamageCalculator m_DamageCalculator;

    // NULL
    private IStatGroup NullStat = new NullStatGroup();
    private IFsm NullFsm = new NullFsm();
    private ISkillEngine NullSkillEngine = new NullSkillEngine();
    private ITagger NullTagger = new NullTagger();
    private ICharacterStatus NullStatus = new NullCharacterStatus();
    private ITargetFinder NullTargetFinder = new NullTargetFinder();
    private IHealth NullHealth = new NullHealth();
    private IMovement NullMovement = new NullMovement();
    private IAnimation NullAnimation = new NullAnimation();
    private IInput NullInput = new NullInput();
    private IDamageCalculator NullDamageCalculator = new NullDamageCalculator();

    [ShowInInspector]
    public IStatGroup Stat => stats ?? NullStat;
    public IFsm Fsm => fsm ?? NullFsm;
    public ISkillEngine Skill => skill ?? NullSkillEngine;
    public ITagger Tagger => tagger ?? NullTagger;
    public ICharacterStatus Status => status ?? NullStatus;
    public ITargetFinder TargetFinder => targetFinder ?? NullTargetFinder;
    public IHealth Health => health ?? NullHealth;
    public IMovement Movement => movement ?? NullMovement;
    public IAnimation Animation => m_Animation ?? NullAnimation;
    public IInput Input => m_Input ?? NullInput;
    public IDamageCalculator DamageCalculator => m_DamageCalculator ?? NullDamageCalculator;

    public Transform Trans => m_Trans;
    public Collider2D Collider => m_Collider;
    public Transform GraphicTrans => m_GraphicTrans != null ? m_GraphicTrans : m_Trans;
    public Transform HealthTrans => m_HealthTrans != null ? m_HealthTrans : m_Trans;
    public Transform CenterTransform => m_CenterTrans;
    public virtual Vector3 CenterPosition => GraphicTrans.position + new Vector3(0, m_Height / 2f, 0);
    public virtual Vector3 TopPosition => GraphicTrans.position + new Vector3(0, m_Height, 0);
    public virtual Vector3 BotPosition => GraphicTrans.position;
    public float Width => m_Width;
    public float Height => m_Height;
    public LayerMask EnemyLayerMask
    {
        get => m_EnemyLayerMask;
        set => m_EnemyLayerMask = value;
    }

    public LayerMask AllyLayerMask
    {
        get => m_AllyLayerMask;
        set => m_AllyLayerMask = value;
    }
    public bool AI
    {
        get { return m_AI; }
        set { m_AI = value; }
    }

    public bool IsDead
    {
        set { m_IsDead = value; }
        get { return m_IsDead; }
    }

    protected virtual void Awake()
    {
        m_Trans = transform;
        m_CenterTrans = new GameObject("Center").transform;
        m_CenterTrans.position = CenterPosition;
        m_CenterTrans.parent = m_Trans;

        stats = new StatGroup();
        fsm = GetComponent<IFsm>();
        movement = GetComponent<IMovement>();
        targetFinder = GetComponent<ITargetFinder>();
        skill = GetComponent<ISkillEngine>();
        health = GetComponent<IHealth>();
        status = GetComponent<ICharacterStatus>();
        m_Animation = GetComponent<IAnimation>();
        m_Input = GetComponent<IInput>();
        m_DamageCalculator = GetComponent<IDamageCalculator>();
    }

    protected virtual void Start()
    {
        Movement.Init(this);
        TargetFinder.Init(this);
        Fsm.Init(this);
        Skill.Init(this);
        Health.Init(this);
        Status.Init(this);
        Animation.Init(this);
        Input.Init(this);
        DamageCalculator.Init(this);

    }
    public void Init()
    {
        Stat.RemoveListener(StatKey.Hp, OnUpdateHealth);
        Stat.AddListener(StatKey.Hp, OnUpdateHealth);
        Health.CurrentHealth = Health.MaxHealth = Stat.GetValue(StatKey.Hp);
        Movement.Speed = Stat.GetValue(StatKey.MovementSpeed);
        Health.Initialized = true;

        var skill = Skill.GetSkill(0);
        if (skill != null)
        {
            // Set Cooldown for first skill
            skill.SetCooldown(new Stat(Stat.GetValue(StatKey.AttackSpeed)));
        }
    }

    private void OnUpdateHealth(float value)
    {
        Health.MaxHealth = value;
    }

    private void Update()
    {
        if (IsActived == false) return;
        Movement.OnUpdate();
        TargetFinder.OnUpdate();
        Fsm.OnUpdate();
        Skill.OnUpdate();
        Status.OnUpdate();
        Input.OnUpdate();
    }

    public void SetActiveCollider(bool active)
    {

    }

    private void OnDestroy()
    {
        Stat.RemoveListener(StatKey.Hp, OnUpdateHealth);
    }
    [Button]
    private void Active(string id)
    {
        ActorFactory.Instance.Bind(id, this);
        ActorFactory.Instance.ApplyStat(id, this, 1);
        GameCore.Event.FireNow(this, ActorSpawnEventArgs.Create(this));
    }

    public void Active()
    {
        IsActived = true;
    }
    public void DeActive()
    {
        IsActived = false;
    }
}
