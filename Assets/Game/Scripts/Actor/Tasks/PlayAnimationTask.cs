﻿using Core;
using Cysharp.Threading.Tasks;
using Game.Tasks;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;

public class PlayAnimationTask : SkillTask
{
    [SerializeField] private SkeletonAnimation m_Skeleton;

    [SerializeField, SpineAnimation(dataField = "m_Skeleton")]
    private string m_Animation;

    [SerializeField, SpineEvent(dataField = "m_Skeleton")]
    private string m_EventName;

    [SerializeField] private bool m_Loop;
    [SerializeField] private bool m_WaitingForCompletion = true;
    [SerializeField] private UnityEvent m_OnEventTrigger;

    private EventData m_EventData;

    public override void Begin()
    {
        base.Begin();
        if (m_EventName.IsNotEmpty())
        {
            m_EventData = m_Skeleton.Skeleton.Data.FindEvent(m_EventName);
            m_Skeleton.AnimationState.Event += OnEvent;
        }

        if (Caster.IsDead)
        {
            IsCompleted = true;
            return;
        }


        if (!m_WaitingForCompletion)
        {
            m_Skeleton.ChangeAnimation(m_Animation, m_Loop);
            IsCompleted = true;
        }
    }
    public override void End()
    {
        base.End();
        m_Skeleton.AnimationState.Event -= OnEvent;
    }
    public override void OnStop()
    {
        base.OnStop();
        m_Skeleton.AnimationState.Event -= OnEvent;
        IsCompleted = true;
    }
    public override void Run()
    {
        base.Run();
        if (Caster.IsDead)
        {
            IsCompleted = true;
            return;
        }
        if (m_Skeleton.EnsureSetAnimation(m_Animation, m_Loop))
        {
            if (m_WaitingForCompletion)
            {
                if (m_Skeleton.IsCurrentAnimationComplete())
                {
                    IsCompleted = true;
                }
            }
        }
    }

    private void OnEvent(TrackEntry trackEntry, Spine.Event e)
    {
        if (!IsRunning || m_EventData != e.Data || !m_Skeleton.IsPlaying(m_Animation)) return;
        IsCompleted = true;
        m_OnEventTrigger?.Invoke();
    }
}