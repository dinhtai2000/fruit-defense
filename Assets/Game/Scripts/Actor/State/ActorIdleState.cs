﻿using Game.Fsm;

public class ActorIdleState : BaseState
{
    public string Animation;
    public bool loop = true;

    public override void Enter()
    {
        base.Enter();
        Actor.Animation.Play(Animation, loop);
    }
}