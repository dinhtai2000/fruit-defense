﻿using Game.Fsm;
using Spine;
using System;

public class ActorDeadState : BaseState
{
    public string Animation;
    public bool loop = true;

    public override void Enter()
    {
        base.Enter();
        Actor.Animation.Play(Animation, loop);
        Actor.Animation.SubscribeComplete(DeadCallback);
    }
    public override void Exit()
    {
        base.Exit();
        Actor.Animation.UnsubcribeComplete(DeadCallback);
    }
    private void OnDestroy()
    {
        Actor.Animation.UnsubcribeComplete(DeadCallback);
    }

    private void DeadCallback(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == Animation)
        {
            GameCore.Event.FireNow(this, ActorDeadEventArgs.Create(Actor));
        }
    }
}