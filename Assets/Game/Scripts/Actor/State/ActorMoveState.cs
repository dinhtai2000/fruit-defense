﻿using Game.Fsm;
using UnityEngine;

public class ActorMoveState : BaseState
{
    public Vector3 m_DirectionAutoMove;
    public string Animation;
    public bool loop = true;

    public override void Enter()
    {
        base.Enter();
        Actor.Animation.Play(Animation, loop);
    }

    public override void Execute()
    {
        base.Execute();
        Actor.Movement.MoveDirection(m_DirectionAutoMove);
    }
}