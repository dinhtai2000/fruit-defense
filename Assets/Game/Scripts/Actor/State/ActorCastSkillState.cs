﻿using Game.Fsm;

public class ActorCastSkillState : BaseState
{
    public override void Enter()
    {
        base.Enter();
        Actor.Movement.LockMovement = true;
    }
    public override void Exit()
    {
        base.Exit();
        Actor.Movement.LockMovement = false;
    }
}