﻿[System.Serializable]
public abstract class BaseDatasave : IDatasave
{
    public string Key;
    public BaseDatasave()
    {

    }
    public BaseDatasave(string key)
    {
        this.Key = key;
    }
    public virtual void Save()
    {
    }
    public abstract void Fix();

    public virtual void NextDay()
    {
    }
}