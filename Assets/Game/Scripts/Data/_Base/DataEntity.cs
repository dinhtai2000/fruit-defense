﻿using BansheeGz.BGDatabase;

[System.Serializable]
public class DataEntity
{
    protected BGEntity entity;
    public DataEntity()
    {

    }
    public DataEntity(BGEntity e)
    {
        this.entity = e;
    }
}