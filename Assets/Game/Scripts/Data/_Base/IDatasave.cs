﻿public interface IDatasave
{
    void Fix();
    void NextDay();
    void Save();
}
