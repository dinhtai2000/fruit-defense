﻿using BansheeGz.BGDatabase;
using Game.Data;
using System.Collections.Generic;

[System.Serializable]
public class CharacterTable : DataTable<string, CharacterEntity>
{
    public override void Get(BGEntity e)
    {
        var entity = new CharacterEntity(e);
        Dictionary.Add(entity.Id, entity);
    }

    public override void GetDatabase()
    {
        DB_Character.ForEachEntity(e => Get(e));
    }
}

[System.Serializable]
public class CharacterEntity : DataEntity
{
    public string Id;
    public float Dmg;
    public float Hp;
    public float Range;
    public float Speed;
    public float AtkSpeed;
    public float iDmg;
    public float iHp;
    public float iRange;
    public float iSpeed;
    public float iAtkSpeed;
    public float CritRate;
    public float CritDmg;
    public float Sensor;

    public List<string> Tag;

    public CharacterEntity(BGEntity e) : base(e)
    {
        Id = e.Get<string>("Id");
        Dmg = e.Get<float>("Dmg");
        Hp = e.Get<float>("Hp");
        Range = e.Get<float>("Range");
        Speed = e.Get<float>("Speed");
        AtkSpeed = e.Get<float>("AtkSpeed");
        iDmg = e.Get<float>("iDmg");
        iHp = e.Get<float>("iHp");
        iRange = e.Get<float>("iRange");
        iSpeed = e.Get<float>("iSpeed");
        iAtkSpeed = e.Get<float>("iAtkSpeed");
        CritRate = e.Get<float>("CritRate");
        CritDmg = e.Get<float>("CritDmg");
        Sensor = e.Get<float>("Sensor");

        Tag = e.Get<List<string>>("Tag");
    }

    public T Bind<T>(string key)
    {
        return entity.Get<T>(key);
    }

    public void Bind(IStatGroup stat)
    {
        stat.AddStat(StatKey.Hp, Hp, 0);
        stat.AddStat(StatKey.Damage, Dmg, 0);
        stat.AddStat(StatKey.MovementSpeed, Speed, 0.5f);
        stat.AddStat(StatKey.AttackRange, Range);
        stat.AddStat(StatKey.ReflectDamageMul, 0);
        stat.AddStat(StatKey.CritRate, CritRate, 0, 1);
        stat.AddStat(StatKey.CritDamage, CritDmg, 0);
        stat.AddStat(StatKey.AttackSpeed, AtkSpeed);
        stat.AddStat(StatKey.Sensor, Sensor);

        stat.CalculateStats();
    }
}