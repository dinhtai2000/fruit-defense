using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : LiveSingleton<DataManager>
{
    public static DatasaveManager Save;
    public static DatabaseManager Base;
    public static DataliveManager Live;

    public TimeSpan EndDayTime;

    public void Init()
    {
#if DEVELOPMENT
        EndDayTime = new TimeSpan(0, 2, 0);
#endif

        DataliveManager.Instance.Init(transform);
        DatabaseManager.Instance.Init(transform);
        DatasaveManager.Instance.Init(transform);
    }

    public void ReloadDatabase()
    {
        Base.Reload();
    }
        
    ~DataManager()
    {
    }

}
