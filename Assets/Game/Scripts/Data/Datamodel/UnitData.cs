﻿[System.Serializable]
public class UnitData
{
    private CharacterEntity characterData;
    private int level;
    private int cost;

    public UnitData(CharacterEntity characterData, int level, int cost)
    {
        this.characterData = characterData;
        this.level = level;
        this.cost = cost;
    }

    public CharacterEntity CharacterData { get => characterData; set => characterData = value; }
    public int Level { get => level; set => level = value; }
    public int Cost { get => cost; set => cost = value; }
}