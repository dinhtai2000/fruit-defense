﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasRenderer))]
public class RaycastArea : UnityEngine.UI.MaskableGraphic {
    protected RaycastArea() {
        useLegacyMeshGeneration = false;
    }

    protected override void OnPopulateMesh(VertexHelper vh) {
        vh.Clear();
    }

    protected override void UpdateGeometry() {
        base.UpdateGeometry();
        gameObject.layer = LayerMask.NameToLayer("UI");
    }
}