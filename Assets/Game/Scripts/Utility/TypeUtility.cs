using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Core {
    public class TypeUtility {
        private static List<Type> _allAssemblyTypes;

        public static List<Type> AllAssemblyTypes {
            get {
                if (_allAssemblyTypes == null) {
                    _allAssemblyTypes = new List<Type>();
                    Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    for (int i = 0; i < assemblies.Length; i++) {
                        _allAssemblyTypes.AddRange(assemblies[i].GetTypes());
                    }
                }

                return _allAssemblyTypes;
            }
        }

        private static List<Type> _assemblyTypes;

        public static List<Type> AssemblyTypes {
            get {
                if (_assemblyTypes == null) {
                    _assemblyTypes = new List<Type>();
                    _assemblyTypes.AddRange(typeof(TypeUtility).Assembly.GetTypes());
                }

                return _assemblyTypes;
            }
        }
    }
}