using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core {
    public class AppHelper {
        public bool CheckAppInstalled(string bundleId) {
#if UNITY_ANDROID
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

            AndroidJavaObject launchIntent = null;

            try {
                launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
            } catch (Exception ex) {
                Debug.LogError("Exception" + ex.Message + "    " + ex.StackTrace);
            }

            return launchIntent != null;
#endif
            return false;
        }

        public void MoveToBackground() {
#if UNITY_ANDROID
            var activity =
                new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
#else
            Application.Quit();
#endif
        }

        public void Quit() {
#if UNITY_ANDROID
            var activity =
                new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
            activity.Call("finish");
#else
            Application.Quit();
#endif
        }

        public void ForceKillProcess() {
#if UNITY_EDITOR || UNITY_IOS
            Application.Quit();
#elif UNITY_ANDROID
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
        }
    }
}