using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ComponentExtensions {
    public static T Clone<T>(this T t, string[] ignoreNames = null) where T : Component {
        return t.Clone<T>(t.gameObject, ignoreNames);
    }

    public static T Clone<T>(this T t, GameObject target, string[] ignoreNames = null) where T : Component {
        Type type = typeof(T);
        T clone = target.AddComponent<T>();
        var fields = type.GetFields();
        foreach (var field in fields) {
            if (field.IsStatic) continue;
            if (ignoreNames != null && ignoreNames.Length > 0) {
                bool ignore = false;
                for (int i = 0; i < ignoreNames.Length; i++) {
                    if (field.Name.Equals(ignoreNames[i])) {
                        ignore = true;
                        break;
                    }
                }

                if (ignore) continue;
            }

            field.SetValue(clone, field.GetValue(t));
        }

        var props = type.GetProperties();
        foreach (var prop in props) {
            if (!prop.CanRead || !prop.CanWrite || prop.Name == "name" || prop.IsDefined(typeof(ObsoleteAttribute), true)) continue;

            if (ignoreNames != null && ignoreNames.Length > 0) {
                bool ignore = false;
                for (int i = 0; i < ignoreNames.Length; i++) {
                    if (prop.Name.Equals(ignoreNames[i])) {
                        ignore = true;
                        break;
                    }
                }

                if (ignore) continue;
            }

            prop.SetValue(clone, prop.GetValue(t, null), null);
        }

        return clone;
    }
}