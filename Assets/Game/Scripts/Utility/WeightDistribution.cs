using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightDistribution {
    private float m_Min;
    private float m_Max;
    private float m_Weight;

    public float Min {
        get { return m_Min; }
    }

    public float Max {
        get { return m_Max; }
    }

    public float Weight {
        set { m_Weight = value; }
        get { return m_Weight; }
    }

    public WeightDistribution(float from, float to, float weight) {
        m_Min = from;
        m_Max = to;
        m_Weight = weight;
    }

    /// <summary>
    /// Parsed from string (2 ; 2.3; 3.0  ; 3.2)
    /// </summary>
    /// <param name="value"></param>
    public WeightDistribution(string value) {
        // Pattern (min, max, weight)
        value = value.Replace("(", "").Replace(")", "").Replace(" ", string.Empty);
        var split = value.Split(';');
        float.TryParse(split[0], out m_Min);
        float.TryParse(split[1], out m_Max);
        float.TryParse(split[2], out m_Weight);
    }
}