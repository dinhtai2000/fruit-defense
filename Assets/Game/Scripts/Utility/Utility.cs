using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Core {
    public class Utility {
        public static class Text {
            private const int StringBuilderCapacity = 1024;
            [ThreadStatic] private static StringBuilder s_CachedStringBuilder;

            public static string Format(string format, object arg0) {
                if (format == null) throw new CoreException("Format is invalid.");
                Utility.Text.CheckCachedStringBuilder();
                Utility.Text.s_CachedStringBuilder.Length = 0;
                Utility.Text.s_CachedStringBuilder.AppendFormat(format, arg0);
                return Utility.Text.s_CachedStringBuilder.ToString();
            }

            public static string Format(string format, object arg0, object arg1) {
                if (format == null) throw new CoreException("Format is invalid.");
                Utility.Text.CheckCachedStringBuilder();
                Utility.Text.s_CachedStringBuilder.Length = 0;
                Utility.Text.s_CachedStringBuilder.AppendFormat(format, arg0, arg1);
                return Utility.Text.s_CachedStringBuilder.ToString();
            }

            public static string Format(string format, object arg0, object arg1, object arg2) {
                if (format == null) throw new CoreException("Format is invalid.");
                Utility.Text.CheckCachedStringBuilder();
                Utility.Text.s_CachedStringBuilder.Length = 0;
                Utility.Text.s_CachedStringBuilder.AppendFormat(format, arg0, arg1, arg2);
                return Utility.Text.s_CachedStringBuilder.ToString();
            }

            public static string Format(string format, params object[] args) {
                if (format == null) throw new CoreException("Format is invalid.");
                if (args == null) throw new CoreException("Args is invalid.");
                Utility.Text.CheckCachedStringBuilder();
                Utility.Text.s_CachedStringBuilder.Length = 0;
                Utility.Text.s_CachedStringBuilder.AppendFormat(format, args);
                return Utility.Text.s_CachedStringBuilder.ToString();
            }

            private static void CheckCachedStringBuilder() {
                if (Utility.Text.s_CachedStringBuilder != null) return;
                Utility.Text.s_CachedStringBuilder = new StringBuilder(1024);
            }
        }

        public static class Random {
            public static System.Random Instance = new System.Random();

            public static string RandomString(int length) {
                var allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
                var chars = new char[length];
                for (var i = 0; i < length; i++) {
                    chars[i] = allowedChars[Instance.Next(0, allowedChars.Length)];
                }

                return new string(chars);
            }

            /// <summary>
            /// Return array of random int numbers which are different each other from min(inclusive) to max(inclusive) using Fisher–Yates shuffle
            /// </summary>
            /// <param name="min"></param>
            /// <param name="max"></param>
            /// <param name="amount">Number of result number</param>
            /// <returns></returns>
            public static int[] RandomDistinctNumber(int min, int max, int amount) {
                if (amount > max - min + 1 || amount <= 0) return null;

                int[] domainArray = new int[max - min + 1];
                int[] resultArray = new int[amount];

                for (int i = 0; i < domainArray.Length - 1; i++) {
                    domainArray[i] = min + i;
                }

                int tmpLength = domainArray.Length;
                for (int k = 0; k < amount; k++) {
                    int domainIndex = UnityEngine.Random.Range(0, tmpLength);
                    resultArray[k] = domainArray[domainIndex];

                    int tmp = domainArray[domainIndex];
                    domainArray[domainIndex] = domainArray[tmpLength - 1];
                    domainArray[tmpLength - 1] = tmp;

                    tmpLength--;
                }

                return resultArray;
            }

            /// <summary>
            /// Non allocation random using Fisher–Yates shuffle
            /// </summary>
            /// <param name="domainArray"></param>
            /// <param name="resultArray"></param>
            /// <returns></returns>
            public static int[] RandomDistinctNumberNonAlloc(int[] domainArray, int[] resultArray) {
                int tmpLength = domainArray.Length;
                for (int k = 0; k < resultArray.Length; k++) {
                    int domainIndex = UnityEngine.Random.Range(0, tmpLength);
                    resultArray[k] = domainArray[domainIndex];

                    int tmp = domainArray[domainIndex];
                    domainArray[domainIndex] = domainArray[tmpLength - 1];
                    domainArray[tmpLength - 1] = tmp;

                    tmpLength--;
                }

                return resultArray;
            }

            public static int[] RandomSubIntArray(int[] arr, int subLength) {
                return RandomDistinctNumber(0, arr.Length - 1, subLength);
            }

            /// <summary>
            /// Random a subset of Component[] that elements are different each other.
            /// </summary>
            /// <param name="components"></param>
            /// <param name="amount"></param>
            /// <returns></returns>
            public static Transform[] RandomSubComponentArray(Transform[] components, int amount) {
                if (amount <= 0 || components == null || amount > components.Length) return null;

                int[] randomIndexs = RandomDistinctNumber(0, components.Length - 1, amount);

                Transform[] resultArray = new Transform[amount];
                for (int i = 0; i < amount; i++) {
                    resultArray[i] = components[randomIndexs[i]];
                }

                return resultArray;
            }

            /// <summary>
            /// Random positive or negative sign
            /// </summary>
            /// <param name="chancePositive">Chance of random positive sign. 0 equals 0%, 1 equals 100%.</param>
            /// <returns>1 if positive, -1 if negative</returns>
            public static int RandomSign(float chancePositive) {
                return UnityEngine.Random.value <= chancePositive ? 1 : -1;
            }

            public static float RandomSignFloat(float chancePositive) {
                return UnityEngine.Random.value <= chancePositive ? 1f : -1f;
            }

            public static bool RandomBoolean(float trueChance) {
                return UnityEngine.Random.value <= trueChance;
            }

            public static bool RandomBoolean() {
                return UnityEngine.Random.value <= 0.5f ? true : false;
            }

            public static int[] GetChanceArray(float[] chanceArray) {
                int[] indexArray = new int[10];

                int tmpIndex = 0;

                for (int i = 0; i < chanceArray.Length; i++) {
                    for (int k = 0; k < chanceArray[i] * 10f; k++) {
                        indexArray[tmpIndex] = i;
                        tmpIndex++;
                    }
                }

                return indexArray;
            }

            public static int RandomIndexWeighted(IList<float> chances) {
                // Calculate sum of chance
                var sumChance = 0f;

                for (var i = 0; i < chances.Count; i++) {
                    sumChance += chances[i];
                }

                var rand = UnityEngine.Random.Range(0f, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int i = 0; i < chances.Count; ++i) {
                    if (chances[i] <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + chances[i];

                    if (rand >= minRange && rand <= maxRange) {
                        result = i;
                        break;
                    }
                }

                return result;
            }

            public static int RandomIndexWeighted(IList<int> chances) {
                // Calculate sum of chance
                var sumChance = 0f;

                for (var i = 0; i < chances.Count; i++) {
                    sumChance += chances[i];
                }

                var rand = UnityEngine.Random.Range(0f, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int i = 0; i < chances.Count; ++i) {
                    if (chances[i] <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + chances[i];

                    if (rand >= minRange && rand <= maxRange) {
                        result = i;
                        break;
                    }
                }

                return result;
            }

            public static int RandomWeighted<T>(IList<T> weightables) where T : IWeightable {
                // Calculate sum of chance
                var sumChance = 0f;

                foreach (var weightable in weightables) {
                    sumChance += weightable.Weight;
                }

                var rand = UnityEngine.Random.Range(0, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int index = 0; index < weightables.Count; ++index) {
                    if (weightables[index].Weight <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + weightables[index].Weight;

                    if (rand >= minRange && rand <= maxRange) {
                        result = index;
                        break;
                    }
                }

                return result;
            }

            public static float RandomWeightedValue(IList<float> weightables) {
                // Calculate sum of chance
                var sumChance = 0f;

                foreach (var weightable in weightables) {
                    sumChance += weightable;
                }

                var rand = UnityEngine.Random.Range(0, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int index = 0; index < weightables.Count; ++index) {
                    if (weightables[index] <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + weightables[index];

                    if (rand >= minRange && rand <= maxRange) {
                        result = index;
                        break;
                    }
                }

                return weightables[result];
            }

            public static int RandomWeightedValue(IList<int> weightables) {
                // Calculate sum of chance
                var sumChance = 0f;

                foreach (var weightable in weightables) {
                    sumChance += weightable;
                }

                var rand = UnityEngine.Random.Range(0, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int index = 0; index < weightables.Count; ++index) {
                    if (weightables[index] <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + weightables[index];

                    if (rand >= minRange && rand <= maxRange) {
                        result = index;
                        break;
                    }
                }

                return weightables[result];
            }

            public static int RandomWeighted<T>(T[] weightables) where T : IWeightable {
                // Calculate sum of chance
                var sumChance = 0f;

                foreach (var weightable in weightables) {
                    sumChance += weightable.Weight;
                }

                var rand = UnityEngine.Random.Range(0, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                var length = weightables.Length;
                for (int index = 0; index < length; ++index) {
                    var w = weightables[index];
                    if (w.Weight <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + w.Weight;

                    if (rand >= minRange && rand <= maxRange) {
                        result = index;
                        break;
                    }
                }

                return result;
            }

            public static int RandomWeighted(IWeightable[] weightables) {
                // Calculate sum of chance
                var sumChance = 0f;

                foreach (var weightable in weightables) {
                    sumChance += weightable.Weight;
                }

                var rand = UnityEngine.Random.Range(0, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                var length = weightables.Length;
                for (int index = 0; index < length; ++index) {
                    var w = weightables[index];
                    if (w.Weight <= 0f) continue;

                    minRange = maxRange;
                    maxRange = minRange + w.Weight;

                    if (rand >= minRange && rand <= maxRange) {
                        result = index;
                        break;
                    }
                }

                return result;
            }

            public static float RandomWeightedRange(WeightDistribution[] distribution) {
                // Calculate sum of chance
                var sumChance = 0f;
                foreach (var d in distribution) {
                    if (d.Weight <= 0f) continue;

                    sumChance += d.Weight;
                }

                var rand = UnityEngine.Random.Range(0f, sumChance);
                var minRange = 0f;
                var maxRange = 0f;
                var result = 0;

                for (int i = 0; i < distribution.Length; ++i) {
                    if (distribution[i].Weight <= 0f) continue;

                    minRange += maxRange;
                    maxRange = minRange + distribution[i].Weight;

                    if (rand >= minRange && rand <= maxRange) {
                        result = i;
                        break;
                    }
                }

                var dis = distribution[result];
                return UnityEngine.Random.Range(dis.Min, dis.Max);
            }

            public static UnityEngine.Object GetRandomArrayElement(UnityEngine.Object[] array) {
                return array[UnityEngine.Random.Range(0, array.Length - 1)];
            }

            public static Vector3 RandomPointInBound2D(Bound2D bound) {
                Vector3 v = Vector3.zero;
                float randX = UnityEngine.Random.Range(bound.XLow, bound.XHigh);
                float randY = UnityEngine.Random.Range(bound.YLow, bound.YHigh);
                v.x = randX;
                v.y = randY;
                return v;
            }

            public static Vector3 RandomPointInBound2D(Bound2D bound, float offsetXLow = 0f, float offsetXHigh = 0f, float offsetYLow = 0f,
                float offsetYHigh = 0f) {
                Vector3 v = Vector3.zero;
                float randX = UnityEngine.Random.Range(bound.XLow + offsetXLow, bound.XHigh + offsetXHigh);
                float randY = UnityEngine.Random.Range(bound.YLow + offsetYLow, bound.YHigh + offsetYHigh);
                v.x = randX;
                v.y = randY;
                return v;
            }

            public static Vector3 Bound2D(Vector3 vec, Bound2D bound, float offsetXLow = 0f, float offsetXHigh = 0f, float offsetYLow = 0f,
                float offsetYHigh = 0f) {
                vec.x = Mathf.Clamp(vec.x, bound.XLow + offsetXLow, bound.XHigh + offsetXHigh);
                vec.y = Mathf.Clamp(vec.y, bound.YLow + offsetYLow, bound.YHigh + offsetYHigh);
                return vec;
            }

            public static Vector2 RandomInsideCircle(Vector2 center, float radius) {
                return RandomInsideCircle(center, 0.01f, radius);
            }

            public static Vector2 RandomInsideCircle(Vector2 center, float innerRadius, float outerRadius) {
                // Ignore center
                if (innerRadius < 0.01f) innerRadius = 0.01f;

                Vector2 point = Vector2.zero;
                float radius = UnityEngine.Random.Range(innerRadius, outerRadius);
                float angle = UnityEngine.Random.value * 2f * Mathf.PI;
                point.x = Mathf.Cos(angle) * radius;
                point.y = Mathf.Sin(angle) * radius;
                Vector2 dest = center + point;
                return dest;
            }
        }
    }
}