﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace VAD {
    [Serializable]
    public class Profile {
        [ReadOnly] public string name;

        [ReadOnly] public Vector2 screenSize;

        public float aspectRatio;
    }
}