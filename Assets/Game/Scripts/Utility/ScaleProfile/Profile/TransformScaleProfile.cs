﻿using System;
using UnityEngine;

namespace VAD {
    public class TransformScaleProfile : BaseProfile<TransformScaleProfile.TransformProfile> {
        [Serializable]
        public class TransformProfile : Profile {
            public Vector3 localScale;
        }

        public new Transform m_transform;

        public override void OnApplyProfile(TransformProfile profile) {
            m_transform.localScale = profile.localScale;
        }

#if UNITY_EDITOR
        private void Reset() {
            m_transform = GetComponent<Transform>();
        }
#endif
    }
}