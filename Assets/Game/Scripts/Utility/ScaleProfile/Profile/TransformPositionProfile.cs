﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VAD {
    public class TransformPositionProfile : BaseProfile<TransformPositionProfile.TransformProfile> {
        [Serializable]
        public class TransformProfile : Profile {
            public Vector3 position;
        }

        public new Transform m_transform;

        public override void OnApplyProfile(TransformProfile profile) {
            m_transform.position = profile.position;
        }

#if UNITY_EDITOR
        private void Reset() {
            m_transform = GetComponent<Transform>();
        }
#endif
    }
}