﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace VAD {
    public class CanvasScalerProfile : BaseProfile<CanvasScalerProfile.AspectProfile> {
        [Serializable]
        public class AspectProfile : Profile {
            public CanvasScaler.ScreenMatchMode screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            public float match = 1;
        }

        public CanvasScaler canvasScaler;

        public override void OnApplyProfile(AspectProfile profile) {
            canvasScaler = GetComponent<CanvasScaler>();
            canvasScaler.screenMatchMode = profile.screenMatchMode;
            canvasScaler.matchWidthOrHeight = profile.match;
        }

#if UNITY_EDITOR
        private void Reset() {
            canvasScaler = GetComponent<CanvasScaler>();
        }
#endif
    }
}