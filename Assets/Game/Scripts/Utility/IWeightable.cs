using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeightable {
    float Weight { get; }
}