using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Core {
    public enum SafeAreaMethodType {
        CanvasBased,
        CameraBased,
    };

    public enum AreaUpdateTiming {
        OnReciveMessage = (1 << 0),
        Awake = (1 << 1),
        OnEnable = (1 << 2),
        Start = (1 << 3),
        Update = (1 << 4),
        FixedUpdate = (1 << 5),
    };

    public class SafeAreaMgr : MonoBehaviour {
        public SafeAreaMethodType ControlType = SafeAreaMethodType.CanvasBased;

        public bool addSoftkeyArea = false;
        [EnumMask] public AreaUpdateTiming UpdateTimming = AreaUpdateTiming.Awake;

        [ShowIf("ControlType", SafeAreaMethodType.CanvasBased)] [SerializeField]
        private Canvas m_MainCanvas;

        [ShowIf("ControlType", SafeAreaMethodType.CanvasBased)] [SerializeField]
        private CanvasPropertyOverrider[] m_CanvasPropertyOverriders;

        [ShowIf("ControlType", SafeAreaMethodType.CameraBased)] [SerializeField]
        private CameraPropertyOverrider[] m_CameraPropertyOverriders;


        private Rect Offset {
            get { return new Rect(0, 0, 0, navigationBarHeight); }
        }

        private static int navigationBarHeight = 0;

        private void UpdateSubCanvasProperty() {
            foreach (var targetCanvas in m_CanvasPropertyOverriders) {
                targetCanvas.UpdateCanvasProperty(m_MainCanvas.sortingOrder, Offset);
            }
        }

        private void UpdateCameraProperty() {
            foreach (var targetCamera in m_CameraPropertyOverriders) {
                targetCamera.UpdateCameraProperty(Offset);
            }
        }

        public void UpdateSafeArea() {
            switch (ControlType) {
                case SafeAreaMethodType.CanvasBased:
                    UpdateSubCanvasProperty();
                    break;
                case SafeAreaMethodType.CameraBased:
                    UpdateCameraProperty();
                    break;
            }
        }

        private void Awake() {
#if UNITY_ANDROID && !UNITY_EDITOR
        if (addSoftkeyArea){
            RunOnAndroidUiThread(GetNavigationBarHeight);
        }
#endif

            m_MainCanvas = GetComponent<Canvas>();
            if (HaveMask(AreaUpdateTiming.Awake)) {
                UpdateSafeArea();
            }
        }

        private void OnEnable() {
            if (HaveMask(AreaUpdateTiming.OnEnable)) {
                UpdateSafeArea();
            }
        }

        private void Start() {
            if (HaveMask(AreaUpdateTiming.Start)) {
                UpdateSafeArea();
            }
        }

        private void Update() {
            if (HaveMask(AreaUpdateTiming.Update)) {
                UpdateSafeArea();
            }
        }

        private void FixedUpdate() {
            if (HaveMask(AreaUpdateTiming.FixedUpdate)) {
                UpdateSafeArea();
            }
        }

        // Utility
        private bool HaveMask(AreaUpdateTiming mask) {
            return ((int) UpdateTimming & (int) mask) != 0;
        }

// =================================================================
// 		Functions 4 Android
// =================================================================

        private static void RunOnAndroidUiThread(System.Action target) {
#if UNITY_ANDROID && !UNITY_EDITOR
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
        using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) {

            activity.Call("runOnUiThread", new AndroidJavaRunnable(target));
        }}
#elif UNITY_EDITOR
            target();
#endif
        }

        private static void GetNavigationBarHeight() {
#if UNITY_ANDROID && !UNITY_EDITOR
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
        using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) {
        using (var window = activity.Call<AndroidJavaObject>("getWindow")) {
        using (var resources = activity.Call<AndroidJavaObject>("getResources")) {
            var resourceId = resources.Call<int>("getIdentifier", "navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = resources.Call<int>("getDimensionPixelSize", resourceId);
            }
        }}}}
#elif UNITY_EDITOR
            navigationBarHeight = 0;
#endif
        }
    }
}