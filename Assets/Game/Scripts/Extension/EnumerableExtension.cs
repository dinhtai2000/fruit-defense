using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class EnumerableExtension {
    public static void AddRange<T>(this IEnumerable<T> enumerationToAddTo, IEnumerable<T> itemsToAdd) {
        var addingToList = enumerationToAddTo.ToList();
        addingToList.AddRange(itemsToAdd);

        // Neither of the following works // 
        enumerationToAddTo.Concat(addingToList);
        // OR
        enumerationToAddTo = addingToList;
        // OR
        enumerationToAddTo = new List<T>(addingToList);
    }
}