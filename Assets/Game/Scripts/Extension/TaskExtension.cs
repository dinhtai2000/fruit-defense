using System;
using System.Threading.Tasks;
using UnityEngine;

public static class TaskExtension {
    public static async void DoNotAwait(this Task task) {
        try {
            await task;
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }
}