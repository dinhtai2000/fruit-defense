using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extension
{
    public static bool Compare(this Vector3 v1, Vector3 v2, double epsilon = 1e-4)
    {
        return Vector3.SqrMagnitude(v1 - v2) <= epsilon;
    }

    public static Quaternion GetQuaternion(this Vector3 v)
    {
        return Quaternion.AngleAxis(Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg, Vector3.forward);
    }
    public static bool IsGreaterOrEqual(this Vector3 local, Vector3 other)
    {
        if (local.x >= other.x && local.y >= other.y && local.z >= other.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool IsLesserOrEqual(this Vector3 local, Vector3 other)
    {
        if (local.x <= other.x && local.y <= other.y && local.z <= other.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static float Length(this Vector3 local)
    {
        return local.magnitude;
    }
}