using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtension {
    /// <summary>
    /// Check if gameobject has layer with name
    /// </summary>
    /// <param name="go"></param>
    /// <param name="layerName"></param>
    /// <returns></returns>
    public static bool CompareLayer(this GameObject go, string layerName) {
        return go.layer == LayerMask.NameToLayer(layerName);
    }

    /// <summary>
    /// Set layer for gameobject and its children
    /// </summary>
    /// <param name="go"></param>
    /// <param name="layer"></param>
    public static void SetLayerRecursively(this GameObject go, int layer) {
        foreach (Transform trans in go.transform.GetComponentsInChildren<Transform>(true)) {
            trans.gameObject.layer = layer;
        }
    }

    /// <summary>
    /// Set layer for gameobject, can include parent and children by layer name
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="layerName"></param>
    /// <param name="includeParent"></param>
    /// <param name="includeChildren"></param>
    public static void SetLayerRecursively(this GameObject go, string layerName) {
        int layer = LayerMask.NameToLayer(layerName);
        SetLayerRecursively(go, layer);
    }
}