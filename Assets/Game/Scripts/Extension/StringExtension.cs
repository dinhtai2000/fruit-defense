using Foundation.Game.Time;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;

public static class StringExtension {
    public static bool IsEmpty(this string str) {
        return string.IsNullOrEmpty(str);
    }

    public static bool IsNotEmpty(this string str) {
        return !string.IsNullOrEmpty(str);
    }

    public static DateTime ToDateTime(this string str) {
        return DateTime.ParseExact(str, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    }

    public static DateTime ToBinaryUniversalDateTime(this string str) {
        if (string.IsNullOrEmpty(str)) return UnbiasedTime.UtcNow;
        return long.TryParse(str, out long binary) ? DateTime.FromBinary(binary).ToUniversalTime() : UnbiasedTime.UtcNow;
    }

    public static string ToTitleCase(this string title) {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
    }

    public static string ToPercentage(this string value) {
        if (float.TryParse(value, out var fValue)) {
            return $"{fValue * 100f}%";
        }

        return value;
    }

    public static bool IsStringFormat(this string value) {
        return value.IsNotEmpty() && Regex.IsMatch(value, "{\\d+}");
    }
}