﻿using Game.Core;

public class ActorDeadEventArgs : BaseEventArgs<ActorDeadEventArgs>
{
    public Actor Owner;

    public static ActorDeadEventArgs Create(Actor owner)
    {
        return new ActorDeadEventArgs { Owner = owner };
    }
}
