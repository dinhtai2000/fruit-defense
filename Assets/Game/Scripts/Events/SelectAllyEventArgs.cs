﻿using Game.Core;
using System;

public class SelectAllyEventArgs : BaseEventArgs<SelectAllyEventArgs>
{
    public UnitData allyData;
    public Actor allyActor;

    public SelectAllyEventArgs(UnitData allyData, Actor allyActor)
    {
        this.allyData = allyData;
        this.allyActor = allyActor;
    }

    public static IEventArgs Create(UnitData unitData, Actor allyActor)
    {
        return new SelectAllyEventArgs(unitData, allyActor);
    }
}