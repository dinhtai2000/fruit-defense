﻿using Game.Core;
using System;

public class DropAllyEventArgs : BaseEventArgs<DropAllyEventArgs>
{
    public Actor allyActor;

    public DropAllyEventArgs(Actor allyActor)
    {
        this.allyActor = allyActor;
    }

    public static IEventArgs Create(Actor allyActor)
    {
        return new DropAllyEventArgs(allyActor);
    }
}