﻿using System.Collections;
using System.Collections.Generic;
using Core;
using Game.Core;
using UnityEngine;

public class HittedSkillEventArgs : BaseEventArgs<HittedSkillEventArgs>
{
    public Actor Caster;
    public Actor Target;
    public int SkillId;

    public HittedSkillEventArgs()
    {
        Caster = null;
        Target = null;
        SkillId = -999;
    }

    public static HittedSkillEventArgs Create(Actor _caster, Actor _target, int _skillId)
    {
        HittedSkillEventArgs eventArgs = new HittedSkillEventArgs();
        eventArgs.Caster = _caster;
        eventArgs.Target = _target;
        eventArgs.SkillId = _skillId;
        return eventArgs;
    }
}