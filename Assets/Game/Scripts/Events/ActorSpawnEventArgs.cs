﻿using Game.Core;

public class ActorSpawnEventArgs : BaseEventArgs<ActorSpawnEventArgs>
{
    public Actor Owner;
    public static ActorSpawnEventArgs Create(Actor owner)
    {
        return new ActorSpawnEventArgs { Owner = owner };
    }
}