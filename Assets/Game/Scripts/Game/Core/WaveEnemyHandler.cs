﻿using com.assets.loader.core;
using com.assets.loader.resources;
using com.mec;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WaveEnemyHandler : MonoSingleton<WaveEnemyHandler>
{
    public Transform spawnerHolder;
    public List<Transform> positions = new List<Transform>();
    public WaveData waveData;

    [Button]
    public void SpawnEnemy()
    {
        foreach (var wave in waveData.Waves)
        {
            Timing.RunCoroutine(_WaveSpawner(wave), gameObject);
        }
    }

    private IEnumerator<float> _WaveSpawner(EnemyWaveData wave)
    {
        yield return Timing.WaitForSeconds(wave.delay);
        var enemy = ActorResourceLoader.Instance.SpawnActor(wave.enemyId);
        enemy.transform.position = positions.RandomElement().position;
        yield return Timing.DeltaTime;
        ActorFactory.Instance.Bind(wave.enemyId, enemy);
        ActorFactory.Instance.ApplyStat(wave.enemyId, enemy, wave.level);
        enemy.Active();
        GameCore.Event.FireNow(this, ActorSpawnEventArgs.Create(enemy));
    }
}

[System.Serializable]
public class WaveData
{
    public List<EnemyWaveData> Waves;
}

[System.Serializable]
public class EnemyWaveData
{
    public string enemyId;
    public float delay;
    public int level;
}