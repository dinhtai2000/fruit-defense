﻿public interface IModule
{
    int Priority { get; }
    void OnInit();
    void OnClose();
    void OnUpdate();
}