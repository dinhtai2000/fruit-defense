﻿using Core;
using Game.Core;
using System;

public abstract class GameCore : MonoSingleton<GameCore>
{
    private bool IsInitialize = false;

    public static IEventMgr Event;
    public static IPoolModule Pool;
    public static ICameraMgr ProCamera;
    protected override void Awake()
    {
        base.Awake();
        DataManager.Instance.Init();
    }

    protected override void InitSingleton()
    {
        base.InitSingleton();
        Init();
    }

    protected virtual void Init()
    {
        Event = ModuleMgr.GetModule<EventMgr>();
        Pool = ModuleMgr.GetModule<PoolManager>();
        ProCamera = ModuleMgr.GetModule<ProCameraMgr>();

        ModuleMgr.Init();
        OnModeStart();
    }

    public abstract void OnModeStart();
    public abstract void OnModeUpdate();
    public abstract void OnModeDestroy();


    protected virtual void Update()
    {
        OnModeUpdate();
        ModuleMgr.Update();
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        OnModeDestroy();
        ModuleMgr.ShutDown();
    }
}