﻿using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;
public class ModuleMgr
{
    private static readonly Dictionary<int, IModule> m_AllModules =
                new Dictionary<int, IModule>();

    public static T GetModule<T>() where T : IModule, new()
    {
        return (T)GetModule(typeof(T));
    }

    public static void Init()
    {
        var orderResult = m_AllModules.OrderBy(x => x.Value.Priority);
        foreach (var item in orderResult)
        {
            item.Value.OnInit();
        }
    }

    public static void Update()
    {
        foreach (var module in m_AllModules)
        {
            module.Value.OnUpdate();
        }
    }

    public static void ShutDown()
    {
        var orderResult = m_AllModules.OrderBy(x => x.Value.Priority);
        foreach (var item in orderResult)
        {
            item.Value.OnClose();
        }
        m_AllModules.Clear();
    }

    private static IModule GetModule(Type type)
    {
        int hashCode = type.GetHashCode();
        IModule module = null;
        if (m_AllModules.TryGetValue(hashCode, out module))
        {
            return module;
        }

        module = CreateModule(type);
        return module;
    }

    private static IModule CreateModule(Type type)
    {
        int hashCode = type.GetHashCode();
        IModule module = (IModule)Activator.CreateInstance(type);
        m_AllModules[hashCode] = module;
        return module;
    }
}