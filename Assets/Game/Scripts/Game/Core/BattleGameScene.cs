﻿using Game.Core;
using UnityEngine;

public class BattleGameScene : BaseScene
{
    [SerializeField] private SOStorageActor m_StorageActor;

    protected override void Awake()
    {
        base.Awake();
        m_StorageActor.Actors.Clear();
        GameCore.Event.Subscribe<ActorSpawnEventArgs>(ActorSpawnEvent);
        GameCore.Event.Subscribe<ActorDeadEventArgs>(ActorDeadEvent);
    }

    private void ActorSpawnEvent(object sender, IEventArgs e)
    {
        var arg = e as ActorSpawnEventArgs;
        if (arg == null) return;
        m_StorageActor.Actors.Add(arg.Owner);
    }

    private void ActorDeadEvent(object sender, IEventArgs e)
    {
        var arg = e as ActorDeadEventArgs;
        if (arg == null) return;
        m_StorageActor.Actors.Remove(arg.Owner);

        Destroy(arg.Owner.gameObject);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameCore.Event.Unsubscribe<ActorSpawnEventArgs>(ActorSpawnEvent);
        GameCore.Event.Unsubscribe<ActorDeadEventArgs>(ActorDeadEvent);
    }

    public override void OnModeStart()
    {
    }

    public override void OnModeUpdate()
    {
    }

    public override void OnModeDestroy()
    {
    }
}