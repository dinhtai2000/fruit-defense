﻿using Game.Core;
using UnityEngine;

public class TouchAllyController : MonoSingleton<TouchAllyController>
{
    private Actor currentAllyTouch;
    private void Start()
    {
        GameCore.Event.Subscribe<SelectAllyEventArgs>(SelectAllyEvent);
        GameCore.Event.Subscribe<DeSelectAllyEventArgs>(DeSelectAllyEvent);
        GameCore.Event.Subscribe<DropAllyEventArgs>(DropAllyEvent);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameCore.Event.Unsubscribe<SelectAllyEventArgs>(SelectAllyEvent);
        GameCore.Event.Unsubscribe<DeSelectAllyEventArgs>(DeSelectAllyEvent);
        GameCore.Event.Unsubscribe<DropAllyEventArgs>(DropAllyEvent);
    }

    private void SelectAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as SelectAllyEventArgs;
        currentAllyTouch = arg.allyActor;
    }

    private void DeSelectAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as DeSelectAllyEventArgs;
    }

    private void DropAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as DropAllyEventArgs;
        if (currentAllyTouch)
        {
            currentAllyTouch.Active();
            currentAllyTouch = null;
        }
    }

    private void Update()
    {
        if (currentAllyTouch == null) return;
        var currentPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        currentPos.z = 0;
        currentAllyTouch.transform.position = currentPos;

        if (Input.GetMouseButtonUp(0))
        {
            GameCore.Event.Fire(this, DropAllyEventArgs.Create(currentAllyTouch));
        }
    }
}