﻿using BeauRoutine;
using com.assets.loader.core;
using com.assets.loader.resources;
using com.mec;
using Cysharp.Threading.Tasks.Triggers;
using Sirenix.OdinInspector;
using UnityEngine.UIElements;

public class ActorResourceLoader : MonoSingleton<ActorResourceLoader>
{
    private IAssetLoader actorLoader;
    protected override void InitSingleton()
    {
        base.InitSingleton();
        actorLoader = new ResourcesAssetLoader();
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        actorLoader.ReleaseAll();
    }

    public Actor SpawnActor(string id)
    {
        var prefab = actorLoader.Load<Actor>("Actor/" + id).Result;
        var actor = GameCore.Pool.Spawn(prefab, transform);
        return actor;
    }

    public Actor SpawnActor(CharacterEntity data, int level)
    {
        var prefab = actorLoader.Load<Actor>("Actor/" + data.Id).Result;
        var actor = GameCore.Pool.Spawn(prefab, transform);
        ActorFactory.Instance.Bind(data, actor);
        ActorFactory.Instance.ApplyStat(data, actor, level);

        GameCore.Event.FireNow(this, ActorSpawnEventArgs.Create(actor));
        return actor;
    }
}