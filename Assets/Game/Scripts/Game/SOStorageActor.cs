﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOStorageActor : ScriptableObject
{
    public List<Actor> Actors;
}