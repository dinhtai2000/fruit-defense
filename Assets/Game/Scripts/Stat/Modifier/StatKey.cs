﻿public enum StatKey
{
    Hp,
    Damage,
    AttackRange,
    AttackSpeed,
    ReflectDamageMul,
    MovementSpeed,
    CritRate,
    CritDamage,
    Level,
    Sensor,
}