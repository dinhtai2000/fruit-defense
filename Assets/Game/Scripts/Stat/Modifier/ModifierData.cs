﻿[System.Serializable]
public class ModifierData
{
    public StatKey AttributeName;
    public StatModifier Modifier;
}