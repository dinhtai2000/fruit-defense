﻿using Sirenix.OdinInspector;
using System;

/// <summary>
/// StatModifier to modifier any value
/// </summary>
[System.Serializable]
public class StatModifier
{
    public EStatMod Type;
    [UnityEngine.SerializeField]
    private float value;
    public object Source;
    [System.NonSerialized]
    private System.Action recalculateValueAction;

    public float Value
    {
        get => value;
        set
        {
            this.value = value;
            RecalculateValueAction?.Invoke();
        }
    }

    public Action RecalculateValueAction { get => recalculateValueAction; set => recalculateValueAction = value; }

    public StatModifier() { }
    public StatModifier(EStatMod mod, float value)
    {
        this.Type = mod;
        this.value = value;
    }
}
