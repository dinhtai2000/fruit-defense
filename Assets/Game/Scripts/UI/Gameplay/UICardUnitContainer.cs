﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICardUnitContainer : MonoBehaviour
{
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private UICardUnitItem cardUnitPrefab;

    private List<UICardUnitItem> unitItems = new List<UICardUnitItem>();

    public void Init(List<UnitData> listUnit)
    {
        Clear();
        unitItems.Clear();
        foreach (var unitData in listUnit)
        {
            var item = GameCore.Pool.Spawn(cardUnitPrefab, scrollRect.content);
            item.SetData(unitData, this);
            unitItems.Add(item);
        }
    }

    private void Clear()
    {
        foreach (var item in unitItems)
        {
            GameCore.Pool.Despawn(item.gameObject);
        }
    }

    [Button]
    private void Test(string id, int level)
    {
        var character = DataManager.Base.Character.Get(id);
        var unitData = new UnitData(character, 1, 0);

        Init(new List<UnitData> { unitData });
    }
}