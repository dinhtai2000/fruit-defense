﻿using BeauRoutine;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UICardUnitItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image unitImage;
    [SerializeField] private TextMeshProUGUI costTxt;
    [SerializeField] private Button button;

    private UnitData unitData;
    private UICardUnitContainer container;
    private void OnEnable()
    {
        button.onClick.AddListener(OnClickCardUnit);
    }
    private void OnDisable()
    {
        button.onClick.RemoveListener(OnClickCardUnit);
    }

    public void SetData(UnitData unitData, UICardUnitContainer container)
    {
        this.container = container;
        this.unitData = unitData;
        costTxt.SetText(unitData.Cost.ToString());
        unitImage.sprite = ResourcesLoader.Instance.GetSprite("Actor", unitData.CharacterData.Id);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Create actor
        var ally = ActorResourceLoader.Instance.SpawnActor(this.unitData.CharacterData, 1);
        GameCore.Event.Fire(this, SelectAllyEventArgs.Create(unitData, ally));
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }

    private void OnClickCardUnit()
    {

    }

}