﻿using Game.Core;
using UnityEngine;

public class GameDropZone : MonoBehaviour
{
    [SerializeField] private GameObject dropZoneVisual;
    private void Start()
    {
        GameCore.Event.Subscribe<SelectAllyEventArgs>(SelectAllyEvent);
        GameCore.Event.Subscribe<DeSelectAllyEventArgs>(DeSelectAllyEvent);
        GameCore.Event.Subscribe<DropAllyEventArgs>(DropAllyEvent);
        dropZoneVisual.SetActive(false);
    }

    private void OnDestroy()
    {
        GameCore.Event.Unsubscribe<SelectAllyEventArgs>(SelectAllyEvent);
        GameCore.Event.Unsubscribe<DeSelectAllyEventArgs>(DeSelectAllyEvent);
        GameCore.Event.Unsubscribe<DropAllyEventArgs>(DropAllyEvent);
    }

    private void SelectAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as SelectAllyEventArgs;
        dropZoneVisual.SetActive(true);
    }

    private void DeSelectAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as DeSelectAllyEventArgs;
        dropZoneVisual.SetActive(false);
    }

    private void DropAllyEvent(object sender, IEventArgs e)
    {
        var arg = e as DropAllyEventArgs;
        dropZoneVisual.SetActive(false);
    }
}