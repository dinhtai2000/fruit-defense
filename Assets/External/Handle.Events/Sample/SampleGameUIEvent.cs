﻿using Game.Core;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace sample.evt
{
    public class SampleGameUIEvent : MonoBehaviour
    {
        public Text text;
        private void Awake()
        {
            EventMgr.Instance.Subscribe<GameStartEventArgs>(GameStartEvent);
            EventMgr.Instance.Subscribe<GameEndEventArgs>(GameEndEvent);
            EventMgr.Instance.Subscribe<EnemyKillEventArgs>(EnemyKillEvent);
        }
        private void OnDestroy()
        {
            EventMgr.Instance.Unsubscribe<GameStartEventArgs>(GameStartEvent);
            EventMgr.Instance.Unsubscribe<GameEndEventArgs>(GameEndEvent);
            EventMgr.Instance.Unsubscribe<EnemyKillEventArgs>(EnemyKillEvent);
        }

        private void GameStartEvent(object sender, IEventArgs e)
        {
            text.text = "Game Start Now!";
        }

        private void GameEndEvent(object sender, IEventArgs e)
        {
            var arg = e as GameEndEventArgs;
            text.text = "Game End With Enemy Killed: " + arg.EnemyKilled;
        }

        private void EnemyKillEvent(object sender, IEventArgs e)
        {
            var arg = e as EnemyKillEventArgs;

            text.text = "Killed: " + arg.enemy.name;
        }
    }
}