﻿using Game.Core;

namespace sample.evt
{
    public class GameEndEventArgs : BaseEventArgs<GameEndEventArgs>
    {
        public int EnemyKilled;

        public GameEndEventArgs(int enemyKilled)
        {
            EnemyKilled = enemyKilled;
        }
    }
}