﻿using Game.Core;
using UnityEngine;

namespace sample.evt
{
    public class EnemySampleEvent : MonoBehaviour
    {
        private void OnMouseDown()
        {
            // Kill Enemy
            EventMgr.Instance.FireNow(this, EnemyKillEventArgs.Create(gameObject));
            Destroy(gameObject);
        }
    }
}