﻿using Game.Core;
using sample.evt;
using System;
using UnityEngine;
using UnityEngine.UI;

public class SampleEventGM : LiveSingleton<SampleEventGM>
{
    public GameObject[] enemies;
    private int enemyKilledAmount = 0;
    private void Start()
    {
        EventMgr.Instance.FireNow(this, new GameStartEventArgs());
        EventMgr.Instance.Subscribe<EnemyKillEventArgs>(EnemyKillEvent);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        EventMgr.Instance.Unsubscribe<EnemyKillEventArgs>(EnemyKillEvent);
    }

    private void EnemyKillEvent(object sender, IEventArgs e)
    {
        enemyKilledAmount++;
        if (enemyKilledAmount >= enemies.Length)
        {
            EventMgr.Instance.FireNow(this, new GameEndEventArgs(enemyKilledAmount));
        }
    }
}