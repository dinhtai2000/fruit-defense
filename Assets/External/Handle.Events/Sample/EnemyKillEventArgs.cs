﻿using Game.Core;
using UnityEngine;

namespace sample.evt
{
    public class EnemyKillEventArgs : BaseEventArgs<EnemyKillEventArgs>
    {
        public GameObject enemy;

        public static EnemyKillEventArgs Create(GameObject e)
        {
            return new EnemyKillEventArgs { enemy = e };
        }
    }
}