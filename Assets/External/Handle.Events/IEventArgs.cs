namespace Game.Core
{
    public interface IEventArgs
    {
        int Id { get; }
    }
}