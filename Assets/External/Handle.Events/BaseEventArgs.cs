﻿namespace Game.Core
{
    public class BaseEventArgs<T> : IEventArgs
    {
        public int Id => typeof(T).GetHashCode();
    }
}